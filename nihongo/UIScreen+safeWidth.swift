//
//  UIScreen+safeWidth.swift
//  nihongo
//
//  Created by Alex on 2018-09-19.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import UIKit

extension UIScreen {
    
    var safeWidth: CGFloat {
        // 2 * 16 is the padding
        return self.bounds.width - 2 * 16
    }
    
}
