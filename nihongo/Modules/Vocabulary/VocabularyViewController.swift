//
//  VocabularyViewController.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import UIKit

protocol VocabularyViewProtocol: class {
    func updateData()
    func showAlert(withTitle title: String, andMessage message: String?)
    var words: [WordVM]! { get set }
}

class VocabularyViewController: UITableViewController, VocabularyViewProtocol {
    
    let configurator: VocabularyConfiguratorProtocol = VocabularyConfigurator()
    var presenter: VocabularyPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        
        self.title = "Vocabulary"
        self.navigationItem.rightBarButtonItem = configureNavbarButton(assetName: "AddButton", dimensions: 20, action: #selector(VocabularyViewController.addButtonClicked(_:)))
        self.navigationItem.leftBarButtonItem = configureNavbarButton(assetName: "SettingsIcon", dimensions: 20, action: #selector(VocabularyViewController.settingsButtonClicked(_:)))
        
        self.tableView.register(WordCell.self, forCellReuseIdentifier: WordCell.reuseIdentifier)
        
        configureIfNecessary()
    }
    
    private var didConfigure = false
    
    private func configureIfNecessary() {
        if !didConfigure {
            presenter.configureView()
            didConfigure = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        configureIfNecessary()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        didConfigure = false
    }
    
    func showAlert(withTitle title: String, andMessage message: String?) {
        self.showSimpleAlert(title: title, message: message)
    }
    
    // MARK: - Navbar buttons
    
    private func configureNavbarButton(assetName: String, dimensions: CGFloat, action: Selector) -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: assetName), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: dimensions, height: dimensions)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: dimensions).isActive = true
        button.heightAnchor.constraint(equalToConstant: dimensions).isActive = true
        button.addTarget(self, action: action, for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
    
    @objc func addButtonClicked(_ sender: UIButton) {
        presenter.addButtonClicked()
    }
    
    @objc func settingsButtonClicked(_ sender: UIButton) {
        presenter.settingsButtonClicked()
    }
    
    // MARK: - Table view delegate & data source
    
    var words: [WordVM]!
    
    func updateData() {
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return words.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WordCell.reuseIdentifier, for: indexPath) as? WordCell else {
            return UITableViewCell()
        }
        cell.setup(with: words[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.wordSelected(words[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            presenter.deleteButtonClicked(for: words[indexPath.row])
        }
    }
    
}
