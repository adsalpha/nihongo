//
//  WordListConfigurator.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol ReadObjectConfiguratorProtocol: class {
    func configure(with viewController: ReadObjectViewController)
}

class WordListConfigurator: ReadObjectConfiguratorProtocol {
    
    func configure(with viewController: ReadObjectViewController) {
        let presenter = ReadObjectPresenter(view: viewController)
        let interactor = ReadObjectInteractor(presenter: presenter)
        let router = ReadObjectRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
    
}

