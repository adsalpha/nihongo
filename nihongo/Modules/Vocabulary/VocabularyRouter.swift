//
//  VocabularyRouter.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import UIKit

protocol VocabularyRouterProtocol: class {
    func showAddScreen()
    func dismissAddScreen()
    func showSettings()
    func dismissSettings()
    func showWordDetails(_ word: WordVM)
}

class VocabularyRouter: VocabularyRouterProtocol {
    
    weak var viewController: VocabularyViewController!
    weak var presenter: VocabularyPresenterProtocol!
    var addScreen: AddUpdateWordViewController?
    var settingsScreen: UINavigationController?
    
    init(viewController: VocabularyViewController) {
        self.viewController = viewController
    }
    
    func showAddScreen() {
        addScreen = AddUpdateWordViewController()
        let configurator: AddUpdateWordConfiguratorProtocol = AddUpdateWordConfigurator()
        configurator.configureToAddWord(with: addScreen!, outputPresenter: presenter)
        viewController.present(addScreen!, animated: true, completion: nil)
    }
    
    func dismissAddScreen() {
        addScreen?.dismiss(animated: true)
        addScreen = nil
    }
    
    func showSettings() {
        settingsScreen = UINavigationController(rootViewController: SettingsViewController())
        viewController.present(settingsScreen!, animated: true, completion: nil)
    }
    
    func dismissSettings() {
        settingsScreen?.dismiss(animated: true)
        settingsScreen = nil
    }
    
    func showWordDetails(_ word: WordVM) {
        let ctrl = WordDetailsViewController()
        let configurator: WordDetailsConfiguratorProtocol = WordDetailsConfigurator()
        configurator.configure(viewController: ctrl, with: word)
        viewController.navigationController?.pushViewController(ctrl, animated: true)
    }
    
}
