//
//  VocabularyPresenter.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol VocabularyPresenterProtocol: class {
    var router: VocabularyRouterProtocol! { set get }
    
    func configureView()
    func dismissAddController()
    func dismissSettingsController()
    
    func addButtonClicked()
    func settingsButtonClicked()
    
    func fetchWords()
    func updateWords(_ words: [Word])
    func wordSelected(_ word: WordVM)
    func deleteButtonClicked(for word: WordVM)
    func cantDeleteWord()
}

class VocabularyPresenter: VocabularyPresenterProtocol {
    
    weak var view: VocabularyViewProtocol!
    var interactor: VocabularyInteractorProtocol!
    var router: VocabularyRouterProtocol!
    
    required init(view: VocabularyViewProtocol) {
        self.view = view
    }
    
    func configureView() {
        fetchWords()
    }
    
    func dismissAddController() {
        router.dismissAddScreen()
    }
    
    func addButtonClicked() {
        router.showAddScreen()
    }
    
    func settingsButtonClicked() {
        router.showSettings()
    }
    
    func dismissSettingsController() {
        router.dismissSettings()
    }
    
    // MARK: - Words
    
    func fetchWords() {
        interactor.fetchWords()
    }
    
    func updateWords(_ words: [Word]) {
        var wordVMs = [WordVM]()
        for word in words {
            wordVMs.append(word.toVM())
        }
        view.words = wordVMs
        view.updateData()
    }
    
    func wordSelected(_ word: WordVM) {
        router.showWordDetails(word)
    }
    
    func deleteButtonClicked(for word: WordVM) {
        interactor.deleteWord(withId: word.id)
    }
    
    func cantDeleteWord() {
        view.showAlert(withTitle: "Can't delete word", andMessage: "Probably a flashcards session is active, finish it and then try again")
    }

}
