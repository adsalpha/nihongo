//
//  WordCell.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import FlexLayout

class WordCell: UITableViewCell {
    
    static let reuseIdentifier = "WordCell"
    
    private let padding: CGFloat = 16
    
    private let primaryScriptLabel = UILabel()
    private let separatorLabel = UILabel()
    private let secondaryScriptLabel = UILabel()
    private let wordTranslationLabel = UILabel()
    
    private func setupLabels() {
        primaryScriptLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        separatorLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        separatorLabel.text = "・"
        secondaryScriptLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        self.separatorInset = .zero
        self.accessoryType = .disclosureIndicator
        
        setupLabels()
        
        contentView.flex.padding(padding).define { (flex) in
            flex.addItem().direction(.row).width(90%).define { (flex) in
                flex.addItem(primaryScriptLabel)
                flex.addItem(separatorLabel).marginLeft(5).marginRight(5)
                flex.addItem(secondaryScriptLabel)
            }
            flex.addItem(wordTranslationLabel).marginTop(padding)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func changeSecondaryVisibility(to visibility: Bool) {
        separatorLabel.isHidden = !visibility
        secondaryScriptLabel.isHidden = !visibility
    }
    
    func setup(with word: WordVM) {
        self.primaryScriptLabel.text = word.kana
        if word.kanji == nil {
            changeSecondaryVisibility(to: false)
        } else {
            changeSecondaryVisibility(to: true)
            secondaryScriptLabel.text = word.kanji!
        }
        self.wordTranslationLabel.text = word.translations.joined(separator: ", ")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
    
    fileprivate func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        contentView.pin.width(size.width)
        layout()
        return contentView.frame.size
    }
    
}
