//
//  VocabularyInteractor.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol VocabularyInteractorProtocol: class {
    func fetchWords()
    func deleteWord(withId id: Int)
}

class VocabularyInteractor: VocabularyInteractorProtocol {
    
    weak var presenter: VocabularyPresenterProtocol!
    let realmService = RealmService.shared
    
    required init(presenter: VocabularyPresenterProtocol) {
        self.presenter = presenter
    }
    
    func fetchWords() {
        self.presenter.updateWords(realmService.getAllWords())
    }
    
    func deleteWord(withId id: Int) {
        if realmService.deletionLock {
            presenter.cantDeleteWord()
        } else {
            realmService.deleteWord(withId: id)
            presenter.updateWords(realmService.getAllWords())
        }
    }
    
}
