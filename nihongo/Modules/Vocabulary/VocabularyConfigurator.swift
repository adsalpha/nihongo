//
//  VocabularyConfigurator.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol VocabularyConfiguratorProtocol: class {
    func configure(with viewController: VocabularyViewController)
}

class VocabularyConfigurator: VocabularyConfiguratorProtocol {
    
    func configure(with viewController: VocabularyViewController) {
        let presenter = VocabularyPresenter(view: viewController)
        let interactor = VocabularyInteractor(presenter: presenter)
        let router = VocabularyRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
        router.presenter = presenter
    }
    
}

