//
//  WordDetailsViewController.swift
//  nihongo
//
//  Created by Alex on 8/7/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import FlexLayout

protocol WordDetailsViewProtocol: class {
    func configure(with: WordVM)
}

class WordDetailsViewController: UITableViewController, WordDetailsViewProtocol {
    
    var presenter: WordDetailsPresenterProtocol!
    let configurator: WordDetailsConfiguratorProtocol = WordDetailsConfigurator()
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavcon()
        self.setupTableView()
        self.setupCells()
    }
    
    private func setupNavcon() {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = Colors.crimsonRed
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit Word", style: .plain, target: self, action: #selector(WordDetailsViewController.editButtonTapped))
    }
    
    private func setupTableView() {
        self.tableView = UITableView(frame: CGRect.zero, style: .grouped)
        self.view.backgroundColor = Colors.tableViewBackgroundGray
        self.tableView.allowsSelection = false
    }
    
    private func setupCells() {
        self.tableView.register(ExampleCell.self, forCellReuseIdentifier: ExampleCell.reuseIdentifier)
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "BasicCell")
    }
    
    var didAlreadyReload = false
    
    override func viewDidAppear(_ animated: Bool) {
        if !didAlreadyReload {
            tableView.reloadData()
            didAlreadyReload = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        didAlreadyReload = false
    }
    
    // MARK: - Word
    
    private var word: WordVM!
    
    // Header view, header height, values
    private var data = [(UIView, CGFloat, Any)]()
    
    func configure(with word: WordVM) {
        self.word = word
        self.data.removeAll()
        
        self.title = word.kanji ?? word.kana
        
        // Word and translations
        let wordView = BigWordView(backgroundColor: Colors.tableViewBackgroundGray)
        wordView.setLabels(primary: word.kana, secondary: word.kanji)
        let sectionTitleView = TableViewSectionTitleView("Translations")
        let bigHeaderView = ComposedHeaderView(bigWordView: wordView, sectionTitleView: sectionTitleView)
        data.append(
            (bigHeaderView,
             bigHeaderView.defaultHeight,
             word.translations)
        )
        
        // Categories
        if word.categories.count > 0 {
            data.append(
                (TableViewSectionTitleView("Categories"),
                 TableViewSectionTitleView.defaultHeight,
                 word.categories)
            )
        }
        
        // Parts of speech
        if word.partsOfSpeech.count > 0 {
            data.append(
                (TableViewSectionTitleView("Parts of Speech"),
                 TableViewSectionTitleView.defaultHeight,
                 word.partsOfSpeech)
            )
        }
        
        // JLPT level
        if word.jlptLevel != nil || word.jlptLevel != "" {
            data.append(
                (TableViewSectionTitleView("JLPT Level"),
                 TableViewSectionTitleView.defaultHeight,
                 [word.jlptLevel!])
            )
        }
        
        // Examples
        if word.examples.count > 0 {
            data.append(
                (TableViewSectionTitleView("Examples"),
                 TableViewSectionTitleView.defaultHeight,
                 word.examples)
            )
        }
    }
    
    // MARK: - Edit button
    
    @objc func editButtonTapped() {
        presenter.editRequested()
    }
    
    // MARK: - Table view delegate & data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        didAlreadyReload = true
        return self.data.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let dataPt = self.data[safe: section] {
            return dataPt.1
        } else { return 0 }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let dataPt = self.data[safe: section] {
            return dataPt.0
        } else { return nil }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dataPt = self.data[safe: section] {
            if let value = dataPt.2 as? [Any] {
                return value.count
            } else if dataPt.2 as? String != nil {
                return 1
            } else { return 0 }
        } else { return 0 }
    }
    
    private func getLabelCell(at indexPath: IndexPath, text: String) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell", for: indexPath)
        cell.textLabel?.text = text
        cell.textLabel?.numberOfLines = 0
        return cell
    }
    
    private func getExampleCell(at indexPath: IndexPath, example: ExampleVM) -> ExampleCell? {
        if let cell = tableView.dequeueReusableCell(withIdentifier: ExampleCell.reuseIdentifier, for: indexPath) as? ExampleCell {
            cell.set(example: example)
            return cell
        } else { return nil }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let dataPt = self.data[safe: indexPath.section] {
            if let value = dataPt.2 as? [String] {
                return getLabelCell(at: indexPath, text: value[indexPath.row])
            } else if let value = dataPt.2 as? String {
                return getLabelCell(at: indexPath, text: value)
            } else if let value = dataPt.2 as? [ExampleVM], let cell = getExampleCell(at: indexPath, example: value[indexPath.row]) {
                return cell
            } else { return UITableViewCell() }
        } else { return UITableViewCell() }
    }

}
