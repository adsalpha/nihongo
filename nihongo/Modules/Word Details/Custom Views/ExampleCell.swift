//
//  ExampleCell.swift
//  nihongo
//
//  Created by Alex on 8/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class ExampleCell: UITableViewCell {
    
    static let reuseIdentifier = "ExampleCell"
    
    private let exampleLabel = UILabel()
    private let exampleTranslationLabel = UILabel()

    private func setupLabels() {
        exampleLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        exampleLabel.numberOfLines = 0
        exampleTranslationLabel.numberOfLines = 0
        exampleTranslationLabel.textColor = Colors.secondaryTextGray
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .white
        
        setupLabels()
        
        self.selectionStyle = .none
        self.separatorInset = .zero
        
        contentView.flex.padding(8, 16, 8, 16).direction(.column).define { (flex) in
            flex.addItem(exampleLabel)
            flex.addItem(exampleTranslationLabel).marginTop(8)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(example: ExampleVM) {
        self.exampleLabel.text = example.jp
        self.exampleTranslationLabel.text = example.en
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
    
    fileprivate func layout() {
        contentView.flex.layout(mode: .adjustHeight)
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        // 1) Set the contentView's width to the specified size parameter
        contentView.pin.width(size.width)
        
        // 2) Layout contentView flex container
        layout()
        
        // Return the flex container new size
        return contentView.frame.size
    }

}
