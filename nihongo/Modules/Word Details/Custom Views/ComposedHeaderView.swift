//
//  ComposedHeaderView.swift
//  nihongo
//
//  Created by Alex on 8/13/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class ComposedHeaderView: UIView {
    
    private let rootFlexContainer = UIView()
    
    var defaultHeight: CGFloat!
    
    // MARK: - Init
    
    init(bigWordView: BigWordView, sectionTitleView: TableViewSectionTitleView) {
        super.init(frame: .zero)
        
        self.defaultHeight = bigWordView.defaultHeight + TableViewSectionTitleView.defaultHeight + 80.0
        
        rootFlexContainer.flex.direction(.column).define {
            flex in
            flex.addItem(bigWordView).margin(40, 16, 40, 16)
            flex.addItem(sectionTitleView)
        }
        
        addSubview(rootFlexContainer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        rootFlexContainer.pin.top().left().right()
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
