//
//  TableViewSectionTitleView.swift
//  nihongo
//
//  Created by Alex on 8/13/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class TableViewSectionTitleView: UIView {

    private let rootFlexContainer = UIView()
    
    private let titleLabel = UILabel()
    
    func configureLabel() {
        titleLabel.backgroundColor = Colors.tableViewBackgroundGray
        titleLabel.font = UIFont.systemFont(ofSize: 13)
        titleLabel.textColor = Colors.tableViewHeaderTextGray
    }
    
    // Based on the design
    static let defaultHeight: CGFloat = 30.0
    
    // MARK: - Init
    
    init(_ sectionTitle: String) {
        super.init(frame: .zero)
        self.backgroundColor = Colors.tableViewBackgroundGray
        
        configureLabel()
        titleLabel.text = sectionTitle.uppercased()
        
        rootFlexContainer.flex.direction(.column).padding(9, 15, 6, 0).define {
            flex in
            flex.addItem(titleLabel)
        }
        
        addSubview(rootFlexContainer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        rootFlexContainer.pin.top().left().right()
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
