//
//  WordDetailsConfigurator.swift
//  nihongo
//
//  Created by Alex on 8/9/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol WordDetailsConfiguratorProtocol: class {
    func configure(viewController: WordDetailsViewController, with word: WordVM)
}

class WordDetailsConfigurator: WordDetailsConfiguratorProtocol {
    
    func configure(viewController: WordDetailsViewController, with word: WordVM) {
        let presenter = WordDetailsPresenter(view: viewController)
        let interactor = WordDetailsInteractor(presenter: presenter)
        let router = WordDetailsRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
        router.presenter = presenter
        
        presenter.word = word
    }
    
}
