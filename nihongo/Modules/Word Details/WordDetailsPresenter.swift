//
//  WordDetailsPresenter.swift
//  nihongo
//
//  Created by Alex on 8/9/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol WordDetailsPresenterProtocol: class {
    var router: WordDetailsRouterProtocol! { set get }
    var word: WordVM! { get set }
    func editRequested()
    func dismissEditControllerAndUpdateWord(_ word: WordVM?)
}

class WordDetailsPresenter: WordDetailsPresenterProtocol {
    
    weak var view: WordDetailsViewProtocol!
    var interactor: WordDetailsInteractorProtocol!
    var router: WordDetailsRouterProtocol!
    
    required init(view: WordDetailsViewProtocol) {
        self.view = view
    }
    
    var word: WordVM! {
        didSet {
            self.view.configure(with: word)
        }
    }
    
    func editRequested() {
        router.showEditScreen(word)
    }
    
    func dismissEditControllerAndUpdateWord(_ word: WordVM?) {
        router.dismissEditScreen()
        if word != nil {
            self.word = word
        }
    }

}
