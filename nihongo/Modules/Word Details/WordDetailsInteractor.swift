//
//  WordDetailsInteractor.swift
//  nihongo
//
//  Created by Alex on 8/9/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol WordDetailsInteractorProtocol: class {}

class WordDetailsInteractor: WordDetailsInteractorProtocol {
    
    weak var presenter: WordDetailsPresenterProtocol!
    
    required init(presenter: WordDetailsPresenterProtocol) {
        self.presenter = presenter
    }
    
}
