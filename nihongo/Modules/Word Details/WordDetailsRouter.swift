//
//  WordDetailsRouter.swift
//  nihongo
//
//  Created by Alex on 8/9/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol WordDetailsRouterProtocol: class {
    func showEditScreen(_ word: WordVM)
    func dismissEditScreen()
}

class WordDetailsRouter: WordDetailsRouterProtocol {
    
    weak var viewController: WordDetailsViewController!
    weak var presenter: WordDetailsPresenterProtocol!
    var editScreen: AddUpdateWordViewController?
    
    init(viewController: WordDetailsViewController) {
        self.viewController = viewController
    }
    
    func showEditScreen(_ word: WordVM) {
        editScreen = AddUpdateWordViewController()
        let configurator: AddUpdateWordConfiguratorProtocol = AddUpdateWordConfigurator()
        configurator.configureToUpdateWord(with: editScreen!, word: word, outputPresenter: presenter)
        viewController.present(editScreen!, animated: true, completion: nil)
    }
    
    func dismissEditScreen() {
        editScreen?.dismiss(animated: true)
        editScreen = nil
    }
    
}
