//
//  CustomizableTextField.swift
//  nihongo
//
//  Created by Alex on 8/20/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import FlexLayout

class CustomizableTextField: UIView, UITextFieldDelegate {
    
    private let rootFlexContainer = UIView()
    
    // MARK: - Caption Label

    private let captionLabel = UILabel()
    
    private func configureCaptionLabel() {
        captionLabel.font = UIFont.systemFont(ofSize: 9)
    }
    
    func setCaption(to text: String) {
        captionLabel.text = text
    }
    
    // MARK: - Add one more label
    
    private let auxiliaryButton = UIButton()
    
    private func configureAuxiliaryLabel() {
        auxiliaryButton.titleLabel?.font = UIFont.systemFont(ofSize: 10)
        auxiliaryButton.titleLabel?.numberOfLines = 3
        auxiliaryButton.titleLabel?.textAlignment = .center
    }
    
    private func setAuxiliaryText(to text: String) {
        auxiliaryButton.setTitle(text, for: .normal)
    }
    
    @objc private func performAuxiliaryAction() {
        auxiliaryAction()
    }
    
    private var auxiliaryAction: () -> () = { return }
    
    // MARK: - Text field
    
    let textField = UITextField()
    
    private func configureTextField() {
        textField.delegate = self
        textField.autocorrectionType = .no
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    // MARK: - Separator
    
    private let separator = UIView()
    
    // MARK: - Un/highlight
    
    func highlight() {
        captionLabel.textColor = Colors.crimsonRed
        separator.backgroundColor = Colors.crimsonRed
        auxiliaryButton.setTitleColor(Colors.crimsonRed, for: .normal)
    }
    
    func unhighlight() {
        captionLabel.textColor = Colors.inactiveInputGray
        separator.backgroundColor = Colors.inactiveInputGray
        auxiliaryButton.setTitleColor(Colors.inactiveInputGray, for: .normal)
    }
    
    // MARK: - Init
    
    init(width: CGFloat = UIScreen.main.safeWidth, canAdd: Bool = false, canRemove: Bool = false, auxiliaryAction: @escaping () -> () = {}) {
        super.init(frame: .zero)
        self.backgroundColor = .white
        
        configureCaptionLabel()
        configureTextField()
        configureAuxiliaryLabel()
        self.auxiliaryAction = auxiliaryAction
        self.auxiliaryButton.addTarget(self, action: #selector(self.performAuxiliaryAction), for: .touchUpInside)
        
        unhighlight()
        
        let textWidth = canAdd || canRemove ? 85 : 100
        let auxiliaryWidth = textWidth == 85 ? 15 : 0
        if canAdd {
            setAuxiliaryText(to: "Add\none\nmore")
        } else if canRemove {
            setAuxiliaryText(to: "Remove")
        }
        
        rootFlexContainer.flex.addItem().define {
            flex in
            flex.addItem().direction(.row).width(width).define {
                flex in
                flex.addItem().direction(.column).width(textWidth%).define {
                    flex in
                    flex.addItem(captionLabel)
                    flex.addItem(textField).marginTop(8)
                    flex.addItem(separator).height(1).marginTop(8)
                }
                flex.addItem(auxiliaryButton).width(auxiliaryWidth%)
            }
        }
        
        addSubview(rootFlexContainer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        rootFlexContainer.pin.top().left().right()
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Text field delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.highlight()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.unhighlight()
        textField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    var whenShouldBeginEditing: () -> () = { return }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        whenShouldBeginEditing()
        return true
    }
    
    var whenDidChange: () -> () = { return }
    
    @objc func textFieldDidChange() {
        whenDidChange()
    }
    
}
