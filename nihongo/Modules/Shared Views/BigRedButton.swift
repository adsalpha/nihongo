//
//  BigRedButton.swift
//  nihongo
//
//  Created by Alex on 8/21/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class BigRedButton: UIView {

    private let rootFlexContainer = UIView()
    
    // MARK: Button
    
    private let button = UIButton()
    
    private func configureButton() {
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = Colors.crimsonRed
        button.layer.cornerRadius = 20
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
    }
    
    func setTitle(to text: String) {
        button.setTitle(text, for: .normal)
    }
    
    // MARK: Callback
    
    var id: Any?
    
    var callback: ((Any?) -> ())?
    
    @objc func buttonClicked() {
        if let callback = callback {
            callback(id)
        }
    }
    
    // MARK: Init
    
    init(width: CGFloat, buttonId: Any? = nil) {
        super.init(frame: .zero)
        
        configureButton()
        
        rootFlexContainer.flex.addItem().define {
            flex in
            flex.addItem(button).width(width).height(45)
        }
        
        self.addSubview(rootFlexContainer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        rootFlexContainer.pin.all(pin.safeArea)
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
