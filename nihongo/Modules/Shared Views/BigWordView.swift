//
//  WordView.swift
//  nihongo
//
//  Created by Alex on 8/13/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import UIKit
import FlexLayout

class BigWordView: UIView {
    
    private let rootFlexContainer = UIView()
    
    private let primaryScriptLabel = UILabel()
    private let secondaryScriptLabel = UILabel()
    
    func configureLabels() {
        self.primaryScriptLabel.font = UIFont.systemFont(ofSize: 48, weight: .semibold)
        self.primaryScriptLabel.textAlignment = .center
        self.secondaryScriptLabel.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        self.secondaryScriptLabel.textAlignment = .center
    }
    
    // Based on the design
    var defaultHeight: CGFloat = 58.0
    
    // MARK: - Init
    
    init(backgroundColor: UIColor, direction: Flex.Direction = .column) {
        super.init(frame: .zero)
        self.backgroundColor = backgroundColor
        
        configureLabels()

        rootFlexContainer.flex.direction(direction).define {
            flex in
            flex.addItem(secondaryScriptLabel)
            flex.addItem(primaryScriptLabel)
        }
        
        addSubview(rootFlexContainer)
    }
    
    func setLabels(primary: String, secondary: String?) {
        if secondary == nil {
            primaryScriptLabel.text = primary
            secondaryScriptLabel.text = secondary
            secondaryScriptLabel.flex.isIncludedInLayout = false
            if defaultHeight > 58 { defaultHeight -= 24 }
        } else {
            primaryScriptLabel.text = secondary
            secondaryScriptLabel.text = primary
            secondaryScriptLabel.flex.isIncludedInLayout = true
            if defaultHeight == 58 { defaultHeight += 24 }
        }
        rootFlexContainer.flex.markDirty()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        rootFlexContainer.pin.top().left().right()
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
