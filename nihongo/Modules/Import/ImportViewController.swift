//
//  ImportViewController.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

protocol ImportViewProtocol: class {
    func showAlert(withTitle title: String, andMessage message: String?)
}

class ImportViewController: UIViewController, ImportViewProtocol, UITextFieldDelegate {

    var presenter: ImportPresenterProtocol!
    let configurator: ImportConfiguratorProtocol = ImportConfigurator()
    
    var mainView: ImportView {
        return self.view as! ImportView
    }
    
    override func loadView() {
        self.view = ImportView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Import"
        self.mainView.setJlptButtonAction(to: jlptButtonClicked(_:))
        self.mainView.setImportButtonAction(to: importButtonClicked(_:))
        configurator.configure(with: self)
    }
    
    func showAlert(withTitle title: String, andMessage message: String?) {
        self.showSimpleAlert(title: title, message: message)
    }
    
    // MARK: - Import Button
    
    func jlptButtonClicked(_ buttonId: Any?) {
        if let level = buttonId as? JapaneseLevel {
            self.presenter.importRequested(for: level)
        }
    }

    func importButtonClicked(_ buttonId: Any?) {
        presenter.importRequested(from: self.mainView.importTextFieldValue ?? "")
    }
    
}
