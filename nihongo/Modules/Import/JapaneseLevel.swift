//
//  JapaneseLevel.swift
//  nihongo
//
//  Created by Alex on 8/20/18.
//  Copyright © 2018 Alex. All rights reserved.
//

enum JapaneseLevel: String {
    case n5
    case n4
    case n3
    case n2
    case n1
}
