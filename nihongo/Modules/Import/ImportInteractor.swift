//
//  ImportInteractor.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol ImportInteractorProtocol: class {
    func processImport(from url: URL)
}

class ImportInteractor: ImportInteractorProtocol {
    
    weak var presenter: ImportPresenterProtocol!
    let processImportService: ProcessImportServiceProtocol = ProcessImportService()
    
    required init(presenter: ImportPresenterProtocol) {
        self.presenter = presenter
    }
    
    func processImport(from url: URL) {
        processImportService.importWordsFromUrl(
            url,
            duplicateErrorHandler: {
                [weak self] words in
                self?.presenter.processDuplicates(words)
        },
            internalErrorHandler: {
                [weak self] error in
                self?.presenter.processInternalError("\(error)")
        },
            successHandler: {
            [weak self] in
            self?.presenter.importWentWell()
        }
        )
    }
    
}
