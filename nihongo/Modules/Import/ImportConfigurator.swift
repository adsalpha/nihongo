//
//  ImportConfigurator.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol ImportConfiguratorProtocol: class {
    func configure(with viewController: ImportViewController)
}

class ImportConfigurator: ImportConfiguratorProtocol {
    
    func configure(with viewController: ImportViewController) {
        let presenter = ImportPresenter(view: viewController)
        let interactor = ImportInteractor(presenter: presenter)
        let router = ImportRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
    
}
