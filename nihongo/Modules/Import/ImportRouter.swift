//
//  ImportRouter.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol ImportRouterProtocol: class {
}

class ImportRouter: ImportRouterProtocol {
    
    weak var viewController: ImportViewController!
    
    init(viewController: ImportViewController) {
        self.viewController = viewController
    }
    
}
