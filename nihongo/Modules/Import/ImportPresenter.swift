//
//  ImportPresenter.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import Sprinter

protocol ImportPresenterProtocol: class {
    var router: ImportRouterProtocol! { set get }
    
    func importRequested(for level: JapaneseLevel)
    func importRequested(from url: String)
    
    func processInternalError(_ error: String)
    func processDuplicates(_ words: [Word])
    func importWentWell()
}

extension JapaneseLevel {
    
    fileprivate func importUrl() -> String {
        let apiBase = try! FormatString("https://s3-eu-west-1.amazonaws.com/nihongo-app/%s.json")
        switch self {
        case .n5: return try! apiBase.print("n5-words")
        case .n4: return try! apiBase.print("n4-words")
        case .n3: return try! apiBase.print("n3-words")
        case .n2: return try! apiBase.print("n2-words")
        case .n1: return try! apiBase.print("n1-words")
        }
    }
    
}

class ImportPresenter: ImportPresenterProtocol {
    
    weak var view: ImportViewProtocol!
    var interactor: ImportInteractorProtocol!
    var router: ImportRouterProtocol!
    
    required init(view: ImportViewProtocol) {
        self.view = view
    }
    
    func importRequested(for level: JapaneseLevel) {
        importRequested(from: level.importUrl())
    }
    
    func importRequested(from url: String) {
        if let url = URL(string: url) {
            interactor.processImport(from: url)
        } else {
            processInternalError("unsupported URL")
        }
    }
    
    func processInternalError(_ error: String) {
        view.showAlert(withTitle: "Internal error", andMessage: "\(error)")
    }
    
    func processDuplicates(_ words: [Word]) {
        // Some bloody magic
        let warning = "The following duplicates have been encountered while processing imports. They have not been added repeatedly. All non-duplicate entries have been added."
        var descriptions = [String]()
        for word in words {
            if word.kanji == nil {
                descriptions.append("\(word.kana) | \(word.translations[0])")
            } else {
                descriptions.append("\(word.kanji!) | \(word.kana) | \(word.translations[0])")
            }
        }
        let maxIndex = descriptions.count > 7 ? 7 : descriptions.count
        var message = "\(warning)\n\n\(descriptions[0..<maxIndex].joined(separator: ",\n"))"
        if maxIndex > 7 {
            message += ",\nand others"
        }
        view.showAlert(withTitle: "Duplicate warning", andMessage: message)
    }
    
    func importWentWell() {
        view.showAlert(withTitle: "Words imported", andMessage: nil)
    }
    
}
