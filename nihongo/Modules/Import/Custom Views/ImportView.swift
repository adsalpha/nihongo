//
//  ImportView.swift
//  nihongo
//
//  Created by Alex on 8/19/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

extension JapaneseLevel {
    
    fileprivate func stringRepr() -> String {
        switch self {
        case .n5: return "N5"
        case .n4: return "N4"
        case .n3: return "N3"
        case .n2: return "N2"
        case .n1: return "N1"
        }
    }
    
}

class ImportView: UIView {

    private let rootFlexContainer = UIView()
    private let contentView = TPKeyboardAvoidingScrollView()
    
    // MARK: - Labels
    
    let introLabel = UILabel()
    let extraInfoLabel = UILabel()
    
    private func configureLabels() {
        introLabel.text = "If you want to use the app to learn new words instead of adding your own ones, you can choose one of the Japanese Language Proficiency Test word lists to import. The basic level is N5, use it if you are just beginning to learn the language. The most advanced is N1. Be aware that the lists for higher levels do not include the words of lower levels."
        introLabel.textAlignment = .center
        introLabel.numberOfLines = 0
        extraInfoLabel.text = "You can continue to add your own words manually if you choose to import a JLPT list. You can also compile and import your own list of words. Please refer to https://nihongovocab.com/import for information on how to do that. When you're ready, paste the link in the text field below and hit Import."
        extraInfoLabel.textAlignment = .center
        extraInfoLabel.numberOfLines = 0
    }
    
    // MARK: - JLPT buttons
    
    private let jlptButtons: [JapaneseLevel: BigRedButton] = [
        .n5: BigRedButton(width: 75, buttonId: JapaneseLevel.n5),
        .n4: BigRedButton(width: 75, buttonId: JapaneseLevel.n4),
        .n3: BigRedButton(width: 75, buttonId: JapaneseLevel.n3),
        .n2: BigRedButton(width: 75, buttonId: JapaneseLevel.n2),
        .n1: BigRedButton(width: 75, buttonId: JapaneseLevel.n1)
    ]
    
    private func configureJlptButtons() {
        for (level, button) in jlptButtons {
            button.id = level
            button.setTitle(to: level.stringRepr())
        }
    }
    
    func setJlptButtonAction(to action: @escaping (Any?) -> ()) {
        for (_, button) in jlptButtons {
            button.callback = action
        }
    }
    
    // MARK: - Import text field
    
    let importTextField = CustomizableTextField()
    
    func configureImportTextField() {
        importTextField.setCaption(to: "HTTPS link to JSON")
        importTextField.textField.placeholder = "https://example.com"
    }
    
    var importTextFieldValue: String? {
        return importTextField.textField.text
    }
    
    // MARK: - Import button
    
    private let importButton = BigRedButton(width: 144)
    
    private func configureImportButton() {
        importButton.setTitle(to: "Import")
    }
    
    func setImportButtonAction(to action: @escaping (Any?) -> ()) {
        importButton.callback = action
    }
    
    // MARK: - Init
        
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .white
        
        configureLabels()
        configureJlptButtons()
        configureImportTextField()
        configureImportButton()
        
        rootFlexContainer.flex.padding(16).addItem().define {
            flex in
            flex.addItem(introLabel)
            flex.addItem().direction(.row).justifyContent(.center).marginTop(40).marginBottom(20).wrap(.wrap).define {
                flex in
                let order: [JapaneseLevel] = [.n5, .n4, .n3, .n2, .n1]
                for type in order {
                    flex.addItem(jlptButtons[type]!).marginLeft(10).marginRight(10).marginBottom(20)
                }
            }
            flex.addItem(extraInfoLabel).marginBottom(20)
            flex.addItem(importTextField).marginBottom(30)
            flex.addItem().direction(.row).define {
                flex in
                flex.justifyContent(.center).addItem(importButton)
            }
        }
        
        contentView.addSubview(rootFlexContainer)
        self.addSubview(contentView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.pin.all(pin.safeArea)
        rootFlexContainer.pin.top().left().right()
        rootFlexContainer.flex.layout(mode: .adjustHeight)
        contentView.contentSize = rootFlexContainer.frame.size
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
