//
//  AddUpdateWordRouter.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol AddUpdateWordRouterProtocol: class {
    var mode: InteractionMode! { get set }
    func dismiss(word: WordVM?)
}

class AddUpdateWordRouter: AddUpdateWordRouterProtocol {
    
    weak var viewController: AddUpdateWordViewController!
    
    init(viewController: AddUpdateWordViewController) {
        self.viewController = viewController
    }
    
    var mode: InteractionMode!
    
    func dismiss(word: WordVM?) {
        switch mode! {
        case .add(let outputPresenter):
            outputPresenter.dismissAddController()
        case .update(let outputPresenter, _):
            outputPresenter.dismissEditControllerAndUpdateWord(word)
        }
    }
    
}
