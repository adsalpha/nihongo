//
//  WordInput.swift
//  nihongo
//
//  Created by Alex on 8/12/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

enum WordInput: String {
    case primaryScript
    case secondaryScript
    case translations
    case categories
    case example
    case exampleTranslation
}
