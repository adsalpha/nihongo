//
//  AddUpdateWordViewController.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

protocol AddUpdateWordViewProtocol: class {
    func setInputs(to inputs: [WordInput: String?])
    func getReadyForNewInputs()
    func showAlert(title: String, message: String?, actionMessage: String?, dismissMessage: String, actionHandler: ((UIAlertAction) -> ())?)
    func setSuggestions(to: [String])
    func hideExampleFields()
}

class AddUpdateWordViewController: UIViewController, AddUpdateWordViewProtocol {
    
    var presenter: AddUpdateWordPresenterProtocol!
    let configurator: AddUpdateWordConfiguratorProtocol = AddUpdateWordConfigurator()
    
    var mainView: AddUpdateWordView {
        return self.view as! AddUpdateWordView
    }
    
    override func loadView() {
        // Bounds height works well enough without subtracting the insets because the view doesn't fill the entire screen and doesn't have a navigation/tab bar
        self.view = AddUpdateWordView(height: UIScreen.main.bounds.height)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.setSubmitButtonAction(callback: addButtonClicked, title: "Submit")
        self.mainView.setCancelButtonAction(#selector(AddUpdateWordViewController.cancelButtonClicked(_:)), for: self)
        self.mainView.whenAskingForSuggestions = {
            [weak self] query in
            self?.presenter.getSuggestions(type: query)
        }
        presenter.configureView()
    }
    
    func setInputs(to inputs: [WordInput: String?]) {
        mainView.setTextFieldContents(to: inputs)
    }

    // MARK: - Buttons
    
    func addButtonClicked(_ buttonId: Any?) {
        let inputs = mainView.getTextFieldInputs()
        presenter.submitRequested(inputs: inputs)
    }
    
    @objc func cancelButtonClicked(_ sender: UIButton) {
        presenter.inputCancelled()
    }
    
    // MARK: - Text fields
    
    func getReadyForNewInputs() {
        mainView.clearTextFields()
    }
    
    func hideExampleFields() {
        mainView.hideExampleFields()
    }
    
    func showAlert(title: String, message: String?, actionMessage: String?, dismissMessage: String, actionHandler: ((UIAlertAction) -> ())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismiss = UIAlertAction(title: dismissMessage, style: .default, handler: nil)
        alert.addAction(dismiss)
        
        if let actionHandler = actionHandler, let actionMessage = actionMessage {
            let secondAction = UIAlertAction(title: actionMessage, style: .default, handler: actionHandler)
            alert.addAction(secondAction)
        } else {
            fatalError("Must specify both action message and handler or neither.")
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func setSuggestions(to suggestions: [String]) {
        self.mainView.setSuggestions(to: suggestions)
    }

}
