//
//  SuggestionCell.swift
//  nihongo
//
//  Created by Alex on 2018-09-01.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import FlexLayout

class SuggestionCell: UIView {

    private let rootFlexContainer = UIView()
    
    private let suggestionLabel = UILabel()
    
    private func setupSuggestionLabel() {
        suggestionLabel.numberOfLines = 3
        suggestionLabel.minimumScaleFactor = 0.5
        suggestionLabel.adjustsFontSizeToFitWidth = true
    }
    
    var text: String? {
        didSet {
            suggestionLabel.text = text
            suggestionLabel.flex.markDirty()
        }
    }
    
    init(height: CGFloat) {
        super.init(frame: .zero)
        
        setupSuggestionLabel()
        
        rootFlexContainer.flex.addItem().direction(.row).alignContent(.center).height(height).padding(5).define {
            flex in
            flex.justifyContent(.center).addItem(suggestionLabel)
        }
        
        self.addSubview(rootFlexContainer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        rootFlexContainer.pin.all()
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
