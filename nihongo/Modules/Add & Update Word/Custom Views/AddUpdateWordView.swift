//
//  AddUpdateWordView.swift
//  nihongo
//
//  Created by Alex on 8/11/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import FlexLayout
import PinLayout
import TPKeyboardAvoiding

extension WordInput {
    
    fileprivate func placeholder() -> String {
        switch self {
        case .primaryScript:
            return "Hiragana or katakana"
        case .secondaryScript:
            return "Leave empty if none or rarely used"
        case .translations:
            return "Separate by comma if more than one"
        case .categories:
            return "For example, 'economics, computer science'"
        case .example:
            return "A sentence where the word is used"
        case .exampleTranslation:
            return "The meaning of the example in English"
        }
    }
    
    fileprivate func caption() -> String {
        switch self {
        case .primaryScript:
            return "Word"
        case .secondaryScript:
            return "Kanji"
        case .translations:
            return "Translations"
        case .categories:
            return "Categories"
        case .example:
            return "Example"
        case .exampleTranslation:
            return "Example Translation"
        }
    }
    
}

class AddUpdateWordView: UIView {
    
    private let rootFlexContainer = UIView()
    private let contentView = TPKeyboardAvoidingScrollView()
    
    // MARK: - Inputs
    
    private let inputBlocks: [WordInput: [CustomizableTextField]] = [
        .primaryScript: [CustomizableTextField()],
        .secondaryScript: [CustomizableTextField()],
        .translations: [CustomizableTextField(canAdd: true)],
        .categories: [CustomizableTextField(canAdd: true)],
        .example: [CustomizableTextField(canAdd: true)],
        .exampleTranslation: [CustomizableTextField()]
    ]
    
    private var inputFlexes: [WordInput: (Flex)] = [:]
    
    private let inputFields: [WordInput: CustomizableTextField] = [
        .primaryScript: CustomizableTextField(),
        .secondaryScript: CustomizableTextField(),
        .translations: CustomizableTextField(),
        .categories: CustomizableTextField(),
        .example: CustomizableTextField(),
        .exampleTranslation: CustomizableTextField()
    ]
    
    // MARK: - Text fields
    
    private func configureTextFields() {
        for (type, tf) in inputFields {
            tf.textField.placeholder = type.placeholder()
            tf.setCaption(to: type.caption())
            tf.whenShouldBeginEditing = {
                [weak self] in
                self?.textFieldTypeToSuggest = type
                self?.askForSuggestions()
            }
            tf.whenDidChange = {
                [weak self] in
                self?.askForSuggestions()
            }
        }
    }
    
    func setTextFieldContents(to values: [WordInput: String?]) {
        for (type, value) in values {
            inputFields[type]!.textField.text = value
        }
    }
    
    func clearTextFields() {
        for (_, tf) in inputFields {
            tf.textField.text = ""
        }
    }
    
    func getTextFieldInputs() -> [WordInput: String?] {
        var inputs = [WordInput: String?]()
        for (type, tf) in inputFields {
            inputs[type] = tf.textField.text
        }
        return inputs
    }
    
    func hideExampleFields() {
        inputFields[.example]!.flex.isIncludedInLayout = false
        inputFields[.exampleTranslation]!.flex.isIncludedInLayout = false
        relayout()
    }
    
    // MARK: - Suggestion view
    
    private var suggestionView: SuggestionView!
    
    private var textFieldTypeToSuggest: WordInput!
    
    private func configureSuggestionView() {
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
        suggestionView = SuggestionView(frame: frame)
        for (_, tfArray) in inputBlocks {
            for tf in tfArray {
                tf.textField.inputAccessoryView = suggestionView
            }
        }
        for (_, tf) in inputFields {
            tf.textField.inputAccessoryView = suggestionView
        }
        suggestionView.whenSuggestionIsSelected = {
            [weak self] suggestion in
            if suggestion != nil || suggestion != "" {
                if let self = self {
                    self.inputFields[self.textFieldTypeToSuggest]!.textField.text = suggestion
                }
            }
        }
    }
    
    func askForSuggestions() {
        let kana = inputFields[.primaryScript]!.textField.text
        let kanji = inputFields[.secondaryScript]!.textField.text
        switch textFieldTypeToSuggest! {
        case .primaryScript:
            if kanji?.count ?? 0 > 2 {
                whenAskingForSuggestions(.kanaForKanji(kanji!))
            } else {
                suggestionView.isHidden = true
            }
        case .secondaryScript:
            if kana?.count ?? 0 > 2 {
                whenAskingForSuggestions(.kanjiForKana(kana!))
            } else {
                suggestionView.isHidden = true
            }
        case .translations:
            if kana?.count ?? 0 > 2 || kanji?.count ?? 0 > 2 {
                whenAskingForSuggestions(.translation(kana, kanji))
            } else {
                suggestionView.isHidden = true
            }
        case .categories:
            if kana?.count ?? 0 > 2 || kanji?.count ?? 0 > 2 {
                whenAskingForSuggestions(.category(kana, kanji))
            } else {
                suggestionView.isHidden = true
            }
        default: suggestionView.isHidden = true
        }
    }
    
    var whenAskingForSuggestions: (SuggestionQuery) -> () = { _ in return }
    
    func setSuggestions(to suggestions: [String]) {
        if suggestions.count == 0 {
            suggestionView.isHidden = true
            suggestionView.emptySuggestions()
        } else {
            suggestionView.isHidden = false
            suggestionView.setSuggestions(to: suggestions)
        }
    }
    
    // MARK: - Cancel button
    
    private let cancelButton = UIButton()
    
    private func configureCancelButton() {
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColor(Colors.crimsonRed, for: .normal)
        cancelButton.contentHorizontalAlignment = .right
        cancelButton.titleLabel?.font = cancelButton.titleLabel?.font.withSize(14)
    }
    
    func setCancelButtonAction(_ action: Selector, for target: Any) {
        cancelButton.addTarget(target, action: action, for: .touchUpInside)
    }
    
    // MARK: - Submit button
    
    private let submitButton = BigRedButton(width: 144)
    
    func setSubmitButtonAction(callback: @escaping (Any?) -> (), title: String = "Submit") {
        submitButton.callback = callback
        submitButton.setTitle(to: title)
    }
    
    // MARK: - Init
    
    private func insertTfsIntoBlock(flex: (Flex), type: WordInput) {
        flex.addItem().marginTop(10).marginBottom(10).define {
            flex in
            inputFlexes[type] = flex
            for tf in inputBlocks[type]! {
                flex.addItem(tf)
            }
        }
    }

    init(height: CGFloat) {
        super.init(frame: .zero)
        self.backgroundColor = .white
        
        configureTextFields()
        configureCancelButton()
        configureSuggestionView()
        
        rootFlexContainer.flex.addItem().padding(16).define {
            flex in
            flex.marginBottom(16).alignContent(.end).addItem(cancelButton)
            flex.addItem().marginBottom(16).define {
                flex in
                let tfWithSuggestionOrder: [WordInput] = [.primaryScript, .secondaryScript, .translations, .categories]
                for type in tfWithSuggestionOrder {
                    flex.addItem().marginTop(10).marginBottom(10).define {
                        flex in
                        inputFlexes[type] = flex
                        for tf in inputBlocks[type]! {
                            flex.addItem(tf)
                        }
                    }
                }
                let exampleOrder: [WordInput] = [.example, .exampleTranslation]
                for type in exampleOrder {
                    flex.addItem().marginTop(10).marginBottom(10).define {
                        flex in
                        inputFlexes[type] = flex
                        for tf in inputBlocks[type]! {
                            flex.addItem(tf)
                        }
                    }
                }
            }
            
            flex.addItem().direction(.row).paddingTop(16).define {
                flex in
                flex.justifyContent(.center).addItem(submitButton)
            }
        }
        
        contentView.addSubview(rootFlexContainer)
        addSubview(contentView)
    }
    
    private func relayout() {
        contentView.pin.all(pin.safeArea)
        rootFlexContainer.pin.top().left().right()
        rootFlexContainer.flex.layout(mode: .adjustHeight)
        contentView.contentSize = rootFlexContainer.frame.size
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        relayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
