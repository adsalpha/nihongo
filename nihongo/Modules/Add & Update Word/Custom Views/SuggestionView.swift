//
//  SuggestionView.swift
//  nihongo
//
//  Created by Alex on 2018-08-26.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import FlexLayout

class SuggestionView: UIView {
    
    private let rootFlexContainer = UIView()
    
    // MARK: - Suggestions
    
    private var suggestionCells: [SuggestionCell] = []
    
    private func setupSuggestionCells(height: CGFloat) {
        for _ in 0..<3 {
            let cell = SuggestionCell(height: height)
            suggestionCells.append(cell)
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SuggestionView.suggestionSelected(_:)))
            cell.addGestureRecognizer(gestureRecognizer)
        }
    }
    
    var whenSuggestionIsSelected: (String?) -> () = { _ in return }
    
    @objc func suggestionSelected(_ sender: UITapGestureRecognizer) {
        if let suggestionCell = sender.view as? SuggestionCell {
            whenSuggestionIsSelected(suggestionCell.text)
        }
    }
    
    func emptySuggestions() {
        for cell in suggestionCells {
            cell.text = ""
            cell.isUserInteractionEnabled = false
        }
    }

    func setSuggestions(to suggestions: [String]) {
        for (index, cell) in suggestionCells.enumerated() {
            if let suggestion = suggestions[safe: index] {
                cell.text = suggestion
                cell.isUserInteractionEnabled = true
            } else {
                cell.text = ""
                cell.isUserInteractionEnabled = false
            }
        }
        self.rootFlexContainer.flex.markDirty()
        relayout()
    }
    
    // MARK: - Init
    
    private func setupBlurredBkg(frame: CGRect) {
        // Blur
        let blurEffect = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.backgroundColor = Colors.suggestionViewGray
        blurView.frame = frame
        self.addSubview(blurView)
        
        // Vibrancy
        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
        let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
        vibrancyView.frame = frame
        blurView.contentView.addSubview(vibrancyView)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupBlurredBkg(frame: frame)
        setupSuggestionCells(height: frame.height)
        rootFlexContainer.flex.addItem().direction(.row).height(frame.height).width(frame.width).justifyContent(.spaceBetween).define {
            flex in
            flex.addItem(suggestionCells[0]).width(33.2%)
            flex.addItem().backgroundColor(.gray).width(0.2%)
            flex.addItem(suggestionCells[1]).width(33.2%)
            flex.addItem().backgroundColor(.gray).width(0.2%)
            flex.addItem(suggestionCells[2]).width(33.2%)
        }
        
        self.addSubview(rootFlexContainer)
    }
    
    private func relayout() {
        rootFlexContainer.pin.all()
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        relayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
