//
//  AddInteractor.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol AddUpdateWordInteractorProtocol: class {
    func addWord(_ word: Word)
    func updateWord(_ word: WordVM, withContentsOf contents: Word)
    func getSuggestions(type: SuggestionQuery) -> Suggestion
    var autoloadExamples: Bool { get }
}

class AddUpdateWordInteractor: AddUpdateWordInteractorProtocol {
    
    weak var presenter: AddUpdateWordPresenterProtocol!
    
    required init(presenter: AddUpdateWordPresenterProtocol) {
        self.presenter = presenter
    }
    
    // MARK: - Database
    
    let realmService: RealmServiceProtocol = RealmService.shared
    
    func addWord(_ word: Word) {
        realmService.addWord(word)
        presenter.wordAdded()
    }
    
    func updateWord(_ word: WordVM, withContentsOf contents: Word) {
        let wordToUpdate = realmService.getWord(identifiedBy: word.id)!
        realmService.updateWord(wordToUpdate, withContentsOf: contents)
        presenter.wordUpdated()
    }
    
    // MARK: - Suggestions
    
    let suggestionService: SuggestionServiceProtocol = SuggestionService()
    
    func getSuggestions(type: SuggestionQuery) -> Suggestion {
        switch type {
        case .kanjiForKana(let kana):
            return suggestionService.getKanjiForKana(kana)
        case .kanaForKanji(let kanji):
            return suggestionService.getKanaForKanji(kanji)
        case .translation(let kana, let kanji):
            return suggestionService.getTranslationsForWord(kana: kana, kanji: kanji)
        case .partsOfSpeech(let kana, let kanji):
            return suggestionService.getPartsOfSpeechForWord(kana: kana, kanji: kanji)
        case .category(let kana, let kanji):
            return suggestionService.getCategoriesForWord(kana: kana, kanji: kanji)
        case .examples(let kana, let kanji):
            return suggestionService.getExampleForWord(kana: kana, kanji: kanji)
        case .jlptLevel(let kana, let kanji):
            return suggestionService.getJlptLevelForWord(kana: kana, kanji: kanji)
        }
    }
    
    // MARK: - Settings
    
    let settingsService = SettingsService.shared
    
    var autoloadExamples: Bool {
        return settingsService.autoloadExamples
    }
    
}
