//
//  AddUpdateWordConfigurator.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol AddUpdateWordConfiguratorProtocol: class {
    func configureToAddWord(with viewController: AddUpdateWordViewController, outputPresenter: VocabularyPresenterProtocol)
    func configureToUpdateWord(with viewController: AddUpdateWordViewController, word: WordVM, outputPresenter: WordDetailsPresenterProtocol)
}

class AddUpdateWordConfigurator: AddUpdateWordConfiguratorProtocol {
    
    private func configureAndGetPresenterAndRouter(viewController: AddUpdateWordViewController) -> (AddUpdateWordPresenterProtocol, AddUpdateWordRouterProtocol) {
        let presenter = AddUpdateWordPresenter(view: viewController)
        let interactor = AddUpdateWordInteractor(presenter: presenter)
        let router = AddUpdateWordRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
        
        return (presenter as AddUpdateWordPresenterProtocol, router as AddUpdateWordRouterProtocol)
    }
    
    func configureToAddWord(with viewController: AddUpdateWordViewController, outputPresenter: VocabularyPresenterProtocol) {
        let (presenter, router) = configureAndGetPresenterAndRouter(viewController: viewController)
        let mode = InteractionMode.add(outputPresenter)
        presenter.setup(mode: mode)
        router.mode = mode
    }
    
    func configureToUpdateWord(with viewController: AddUpdateWordViewController, word: WordVM, outputPresenter: WordDetailsPresenterProtocol) {
        let (presenter, router) = configureAndGetPresenterAndRouter(viewController: viewController)
        let mode = InteractionMode.update(outputPresenter, word)
        presenter.setup(mode: mode)
        router.mode = mode
    }

}
