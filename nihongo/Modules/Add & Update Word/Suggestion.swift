//
//  Suggestion.swift
//  nihongo
//
//  Created by Alex on 8/24/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

enum SuggestionQuery {
    // Optional kana for real-time suggestions, non-optional for submission processing
    case kanjiForKana(String)
    case kanaForKanji(String)
    case translation(String?, String?)
    case partsOfSpeech(String, String?)
    case category(String?, String?)
    case examples(String, String?)
    case jlptLevel(String, String?)
}

enum Suggestion {
    case kanjiForKana([String])
    case kanaForKanji([String])
    case translation([String])
    case partsOfSpeech([String])
    case category([String])
    case examples([(String, String)])
    case jlptLevel(String?)
    
    func getSuggestion() -> Any {
        switch self {
        case .kanjiForKana(let s),
             .kanaForKanji(let s),
             .translation(let s),
             .partsOfSpeech(let s),
             .category(let s):
            return s
        case .examples(let s):
            return s
        case .jlptLevel(let s):
            return s as Any
        }
    }
}
