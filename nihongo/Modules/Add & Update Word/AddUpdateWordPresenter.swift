//
//  AddUpdateWordPresenter.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import UIKit

protocol AddUpdateWordPresenterProtocol: class {
    var router: AddUpdateWordRouterProtocol! { set get }
    func setup(mode: InteractionMode)
    func getSuggestions(type: SuggestionQuery)
    func submitRequested(inputs: [WordInput: String?])
    func wordAdded()
    func wordUpdated()
    func inputCancelled()
    
    func configureView()
}

enum InteractionMode {
    case add(VocabularyPresenterProtocol)
    case update(WordDetailsPresenterProtocol, WordVM)
}

class AddUpdateWordPresenter: AddUpdateWordPresenterProtocol {
    
    weak var view: AddUpdateWordViewProtocol!
    var interactor: AddUpdateWordInteractorProtocol!
    var router: AddUpdateWordRouterProtocol!

    required init(view: AddUpdateWordViewProtocol) {
        self.view = view
    }
    
    private var inputs = [WordInput: String?]()
    
    // MARK: - Mode
    
    private var mode: InteractionMode!
    
    func setup(mode: InteractionMode) {
        switch mode {
        case .add(_):
            self.mode = mode
        case .update(_, let word):
            self.mode = mode
            self.wordToEdit = word
        }
    }
    
    // MARK: - Suggestions
    
    func getSuggestions(type: SuggestionQuery) {
        let suggestions = interactor.getSuggestions(type: type)
        if let rawSuggestion = suggestions.getSuggestion() as? [String] {
            view.setSuggestions(to: rawSuggestion)
        }
    }
    
    // MARK: - Update mode
    
    private var isInitialShow = true
    
    private var wordToEdit: WordVM? {
        didSet {
            if isInitialShow {
                if let word = wordToEdit {
                    inputs = [
                        .primaryScript: word.kana,
                        .secondaryScript: word.kanji,
                        .translations: word.translations.joined(separator: ", "),
                        .categories: word.categories.joined(separator: ", "),
                        .example: word.examples.count == 0 ? nil : word.examples[0].jp,
                        .exampleTranslation: word.examples.count == 0 ? nil : word.examples[0].en
                    ]
                    view.setInputs(to: inputs)
                }
                isInitialShow = false
            }
        }
    }
    
    // MARK: - From view
    
    func configureView() {
        if interactor.autoloadExamples {
            view.hideExampleFields()
        }
    }
    
    private func inputIsEmpty(_ input: String?) -> Bool {
        return input == nil || input == ""
    }
    
    private func showIncompleteWordAlert() {
        view.showAlert(title: "Error", message: "You must specify the word and its translation", actionMessage: "Close", dismissMessage: "Continue adding word") {
            [weak self] _ in
            self?.inputCancelled()
        }
    }
    
    private func showIncompleteExamplesAlert() {
        view.showAlert(title: "Error", message: "Every example must have a translation", actionMessage: "Close", dismissMessage: "Continue adding word") {
            [weak self] _ in
            self?.inputCancelled()
        }
    }
    
    private func parseCSV(_ csvString: String) -> [String] {
        var parsed = [String]()
        let substrings = csvString.split(separator: ",")
        for substring in substrings {
            parsed.append(String(substring).trimmingCharacters(in: .whitespacesAndNewlines))
        }
        return parsed
    }
    
    func submitRequested(inputs: [WordInput: String?]) {
        self.inputs = inputs
        
        // Two optionals for each key - one from the dict key which I implicitly unwrap, another one with the text field value
        let kana = inputs[.primaryScript]!
        let translations = inputs[.translations]!
        
        guard !inputIsEmpty(kana) && !inputIsEmpty(translations) else {
            showIncompleteWordAlert()
            return
        }
        
        let parsedTranslations = parseCSV(translations!)
        
        let kanji = inputIsEmpty(inputs[.secondaryScript]!) ? nil : inputs[.secondaryScript]!!
        
        var parsedCategories = [String]()
        
        if !inputIsEmpty(inputs[.categories]!) {
            parsedCategories = parseCSV(inputs[.categories]!!)
        }
        
        var examples = [Example]()
        
        if interactor.autoloadExamples {
            let exampleSuggestion = interactor.getSuggestions(type: .examples(kana!, kanji))
            if let rawSuggestion = exampleSuggestion.getSuggestion() as? [(String, String)] {
                for record in rawSuggestion {
                    examples.append(Example(jp: record.0, en: record.1))
                }
            }
        } else {
            let examplesJp = inputIsEmpty(inputs[.example]!) ? [String]() : [inputs[.example]!!]
            let examplesEn = inputIsEmpty(inputs[.exampleTranslation]!) ? [String]() : [inputs[.exampleTranslation]!!]
            
            guard examplesJp.count == examplesEn.count else {
                showIncompleteExamplesAlert()
                return
            }
            
            for (jp, en) in zip(examplesJp, examplesEn) {
                examples.append(Example(jp: jp, en: en))
            }
        }
        
        let partsOfSpeech = interactor.getSuggestions(type: .partsOfSpeech(kana!, kanji)).getSuggestion() as? [String] ?? [String]()
        
        let jlptLevel = interactor.getSuggestions(type: .jlptLevel(kana!, kanji)).getSuggestion() as? String
        
        let word = Word(
            kana: kana!,
            kanji: kanji,
            translations: parsedTranslations,
            partsOfSpeech: partsOfSpeech,
            categories: parsedCategories,
            examples: examples,
            jlptLevel: jlptLevel,
            visible: true
        )
        
        switch mode! {
        case .add:
            interactor.addWord(word)
        case .update:
            let id = wordToEdit!.id
            wordToEdit = word.toVM()
            wordToEdit!.id = id
            interactor.updateWord(wordToEdit!, withContentsOf: word)
        }
        
    }
    
    func inputCancelled() {
        router.dismiss(word: nil)
    }
    
    // MARK: - From interactor
    
    func wordAdded() {
        view.getReadyForNewInputs()
        view.showAlert(title: "Word Added", message: nil, actionMessage: "Close", dismissMessage: "Add another word") {
            [weak self] _ in
            self?.inputCancelled()
        }
    }
    
    func wordUpdated() {
        router.dismiss(word: wordToEdit)
    }
    
}
