//
//  TabBarController.swift
//  nihongo
//
//  Created by Alex on 8/17/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        let vocabularyViewController = VocabularyViewController()
        vocabularyViewController.tabBarItem = UITabBarItem(title: "Vocabulary", image: UIImage(named: "VocabularyIcon"), tag: 0)
        
        let quizViewController = QuizViewController()
        quizViewController.tabBarItem = UITabBarItem(title: "Quiz", image: UIImage(named: "QuizIcon"), tag: 1)
        
        let viewControllerList = [vocabularyViewController, quizViewController]
        
        tabBar.tintColor = Colors.crimsonRed
        
        viewControllers = viewControllerList.map { UINavigationController(rootViewController: $0) }
    }
    
}
