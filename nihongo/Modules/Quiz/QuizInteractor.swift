//
//  QuizInteractor.swift
//  nihongo
//
//  Created by Alex on 8/1/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol QuizInteractorProtocol: class {
    func getAnotherWord()
    func userGuessedRightFromTheFirstTime()
    func userGuessedWrong()
}

class QuizInteractor: QuizInteractorProtocol {
    
    weak var presenter: QuizPresenterProtocol!
    var realmService = RealmService.shared
    var flashcardWordProvider: FlashcardWordProviderProtocol = FlashcardWordProvider()
    
    required init(presenter: QuizPresenterProtocol) {
        self.presenter = presenter
    }
    
    // In the beginning there obviously were no words
    var thereWereNoWords: Bool = true
    
    func getAnotherWord() {
        if thereWereNoWords {
            flashcardWordProvider.updateWords()
        }
        
        let newWord = flashcardWordProvider.getNewWord()
        realmService.deletionLock = true
        if let correctWord = newWord.0 {
            thereWereNoWords = false
            if let randomWords = flashcardWordProvider.get3RandomWords().0 {
                presenter.processNewWords(correct: correctWord, incorrect: randomWords)
            }
        } else {
            realmService.deletionLock = false
            thereWereNoWords = true
            switch newWord.1! {
            case .notEnoughWords:
                presenter.notEnoughWords()
            case .noMoreWords:
                presenter.finishSession()
            }
        }
    }
    
    func userGuessedRightFromTheFirstTime() {
        flashcardWordProvider.currentWordLearned()
    }
    
    func userGuessedWrong() {
        flashcardWordProvider.currentWordNotLearned()
    }
    
}
