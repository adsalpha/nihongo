//
//  QuizRouter.swift
//  nihongo
//
//  Created by Alex on 8/1/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol QuizRouterProtocol: class {
}

class QuizRouter: QuizRouterProtocol {
    
    weak var viewController: QuizViewController!
    
    init(viewController: QuizViewController) {
        self.viewController = viewController
    }
    
}
