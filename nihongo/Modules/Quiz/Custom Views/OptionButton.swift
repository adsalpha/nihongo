//
//  OptionButton.swift
//  nihongo
//
//  Created by Alex on 8/15/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import FlexLayout

class OptionButton: UIView {

    private let rootFlexContainer = UIView()
    
    // MARK: - Color
    
    func setColor(to borderColor: UIColor) {
        self.rootFlexContainer.layer.borderColor = borderColor.cgColor
    }
    
    // MARK: - Accessory
    
    private let accessoryView = UIImageView()
    
    func setAccessory(to image: UIImage?) {
        accessoryView.image = image
    }
    
    // MARK: - Text label
    
    private let textLabel = UILabel()
    
    func setupTextLabel() {
        textLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
    }
    
    func setOption(to option: String) {
        textLabel.text = option
    }
    
    // MARK: - Init
    
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .white
        self.rootFlexContainer.layer.borderWidth = 1
        self.rootFlexContainer.layer.cornerRadius = 10
        
        setupTextLabel()
        
        rootFlexContainer.flex.paddingLeft(16).minHeight(55).shrink(1).direction(.row).define {
            flex in
            flex.addItem(textLabel).width(85%)
            flex.addItem(accessoryView).height(20).width(20).alignSelf(.center)
        }
        
        addSubview(rootFlexContainer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        rootFlexContainer.pin.all(pin.safeArea)
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
