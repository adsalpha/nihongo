//
//  QuestionView.swift
//  nihongo
//
//  Created by Alex on 8/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import FlexLayout

class QuestionView: UIView {

    private let rootFlexContainer = UIView()

    // MARK: - Option buttons

    private let optionButtons = [OptionButton(), OptionButton(), OptionButton(), OptionButton()]
    
    func setupButtons() {
        for button in optionButtons {
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(QuestionView.buttonClicked(_:)))
            button.addGestureRecognizer(gestureRecognizer)
            button.isUserInteractionEnabled = true
        }
    }

    func setButtonOptions(to options: [String]) {
        for (button, option) in zip(optionButtons, options) {
            button.setOption(to: option)
        }
    }
    
    func resetAllButtons() {
        for button in optionButtons {
            button.isUserInteractionEnabled = true
            button.setColor(to: Colors.optionButtonGray)
            button.setAccessory(to: nil)
        }
    }
    
    var buttonAction: (_ optionIndex: Int) -> () = { _ in return }
    
    @objc func buttonClicked(_ sender: UIGestureRecognizer) {
        if let button = sender.view as? OptionButton {
            let index = optionButtons.firstIndex(of: button)
            buttonAction(index!)
        }
    }
    
    func deactivateButton(at optionIndex: Int) {
        optionButtons[optionIndex].setColor(to: Colors.crimsonRed)
        optionButtons[optionIndex].setAccessory(to: UIImage(named: "CheckboxRedIncorrect"))
        optionButtons[optionIndex].isUserInteractionEnabled = false
    }
    
    // MARK: - Description label

    private let descriptionLabel = UILabel()
    
    func setupDescriptionLabel() {
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 0
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
    }
    
    func setDescription(question: String) {
        descriptionLabel.text = question
    }
    
    // MARK: - Word
    
    private var bigWordView = BigWordView(backgroundColor: .white, direction: .columnReverse)
    
    func setWord(primary: String, secondary: String?) {
        bigWordView.setLabels(primary: primary, secondary: secondary)
        rootFlexContainer.flex.markDirty()
    }

    // MARK: - Init

    init(height: CGFloat) {
        super.init(frame: .zero)
        self.backgroundColor = .white
        
        setupButtons()
        setupDescriptionLabel()
        
        rootFlexContainer.flex.padding(height * 0.075, 16, height * 0.025, 16).height(height).justifyContent(.spaceBetween).define {
            flex in
            flex.addItem().height(30%).justifyContent(.spaceEvenly).define {
                flex in
                flex.addItem(bigWordView).height(60%)
                flex.addItem(descriptionLabel).height(40%)
            }
            flex.addItem().justifyContent(.spaceEvenly).height(70%).define {
                flex in
                for button in optionButtons {
                    flex.addItem(button)
                }
            }
        }
        
        addSubview(rootFlexContainer)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        rootFlexContainer.pin.all(pin.safeArea)
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
