//
//  QuizView.swift
//  nihongo
//
//  Created by Alex on 8/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class QuizView: UIView {

    private let rootFlexContainer = UIView()
    
    var questionView: QuestionView!
    var finishedQuizView: FinishedQuizView!
    
    init() {
        super.init(frame: .zero)
        self.backgroundColor = .white
        addSubview(rootFlexContainer)
    }
    
    private var height: CGFloat!
    
    func configureWithHeight(_ height: CGFloat) {
        self.height = height
        questionView = QuestionView(height: height)
    }
    
    func showQuestion() {
        if finishedQuizView != nil {
            finishedQuizView.removeFromSuperview()
        }
        if rootFlexContainer.subviews.firstIndex(of: questionView) == nil {
            rootFlexContainer.flex.define {
                flex in
                flex.addItem(questionView)
            }
        }
        reload()
    }
    
    func showFinished(canStartOver: Bool) {
        questionView.removeFromSuperview()
        if finishedQuizView != nil {
            finishedQuizView.removeFromSuperview()
        }
        
        finishedQuizView = FinishedQuizView(height: height, canStartOver: canStartOver)
        rootFlexContainer.flex.define {
            flex in
            flex.addItem(finishedQuizView)
        }
        reload()
    }
    
    func reload() {
        rootFlexContainer.flex.markDirty()
        relayout()
    }
    
    private func relayout() {
        rootFlexContainer.pin.all(pin.safeArea)
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        relayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
