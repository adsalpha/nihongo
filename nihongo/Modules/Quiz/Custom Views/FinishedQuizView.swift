//
//  FinishedQuizView.swift
//  nihongo
//
//  Created by Alex on 8/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class FinishedQuizView: UIView {

    private let rootFlexContainer = UIView()
    
    // MARK: - Description label
    
    private let descriptionLabel = UILabel()
    
    private func setupDescriptionLabel() {
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 0
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.font = UIFont.systemFont(ofSize: 30)
    }
    
    func setDescriptionText(_ text: String) {
        descriptionLabel.text = text
        descriptionLabel.flex.markDirty()
    }
    
    // MARK: - Start over button
    
    private let startOverButton = UIButton()
    
    private func setupStartOverButton() {
        startOverButton.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        startOverButton.setTitleColor(Colors.crimsonRed, for: .normal)
        startOverButton.setTitle("Start over", for: .normal)
    }
    
    func setStartOverButtonAction(_ action: Selector, forTarget target: Any) {
        startOverButton.addTarget(target, action: action, for: .touchUpInside)
    }
    
    // MARK: - Init
    
    init(height: CGFloat, canStartOver: Bool) {
        super.init(frame: .zero)
        self.backgroundColor = .white
        
        setupDescriptionLabel()
        if canStartOver {
            setupStartOverButton()
        }
        
        rootFlexContainer.flex.direction(.column).height(height).justifyContent(.center).padding(16).define {
            flex in
            flex.addItem(descriptionLabel)
            if canStartOver {
                flex.addItem(startOverButton).marginTop(20)
            }
        }
        
        addSubview(rootFlexContainer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        relayout()
    }
    
    private func relayout() {
        rootFlexContainer.pin.all(pin.safeArea)
        rootFlexContainer.flex.layout(mode: .adjustHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
