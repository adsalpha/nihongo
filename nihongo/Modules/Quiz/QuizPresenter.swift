//
//  QuizPresenter.swift
//  nihongo
//
//  Created by Alex on 8/1/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol QuizPresenterProtocol: class {
    var router: QuizRouterProtocol! { get set }
    func optionClicked(withIndex index: Int)
    func processNewWords(correct: Word, incorrect: [Word])
    func finishSession()
    func notEnoughWords()
    func configureView()
    func startOverButtonClicked()
}

fileprivate enum QuestionType: String {
    case kana = "What is the correct kana?"
    case wordTranslation = "What does this word mean?"
}

fileprivate enum SessionStatus {
    case readyToStart
    case inProcess
    case noWordsAvailable
    case finished
}

class QuizPresenter: QuizPresenterProtocol {
    
    weak var view: QuizViewProtocol!
    var interactor: QuizInteractorProtocol!
    var router: QuizRouterProtocol!
    
    required init(view: QuizViewProtocol) {
        self.view = view
    }
    
    // MARK: - Setup
    
    private var sessionStatus: SessionStatus = .noWordsAvailable
    
    func configureView() {
        if [.readyToStart, .noWordsAvailable].contains(sessionStatus) {
            interactor.getAnotherWord()
        }
    }

    func startOverButtonClicked() {
        sessionStatus = .readyToStart
        interactor.getAnotherWord()
    }
    
    // MARK: - View interaction handling
    
    private var correctOptionIndex: Int!
    private var lastOptionWrong: Bool = false
    private var questionType: QuestionType!
    
    func optionClicked(withIndex index: Int) {
        if index == correctOptionIndex {
            switch questionType! {
            case .kana:
                checkWordTranslation()
            case .wordTranslation:
                if !lastOptionWrong {
                    interactor.userGuessedRightFromTheFirstTime()
                }
                finishWord()
            }
        } else {
            if !lastOptionWrong {
                lastOptionWrong = true
                interactor.userGuessedWrong()
            }
            view.deactivateOption(at: index)
        }
    }
    
    // MARK: - New word set
    
    private var correct: Word!
    private var incorrect: [Word]!
    
    func processNewWords(correct: Word, incorrect: [Word]) {
        // In case it's after the finished state screen
        view.showQuestion()
        sessionStatus = .inProcess

        correctOptionIndex = Int.random(in: 0...3)
        lastOptionWrong = false
        questionType = .kana
        
        self.correct = correct
        self.incorrect = incorrect
        
        if correct.kanji == nil {
            checkWordTranslation()
        } else {
            var options = [String]()
            for word in incorrect {
                options.append(word.kana)
            }
            options.insert(correct.kana, at: correctOptionIndex)
            
            view.updateQuestionFields(primary: correct.kanji!, secondary: nil, options: options, questionDescription: questionType.rawValue)
        }
    }
    
    func checkWordTranslation() {
        correctOptionIndex = Int.random(in: 0...3)
        questionType = .wordTranslation
        
        var options = [String]()
        for word in incorrect {
            options.append(word.translations.randomElement()!)
        }
        options.insert(correct.translations.randomElement()!, at: correctOptionIndex)
        
        view.updateQuestionFields(primary: correct.kana, secondary: correct.kanji, options: options, questionDescription: questionType.rawValue)
    }
    
    func finishWord() {
        interactor.getAnotherWord()
    }
    
    // MARK: - Errors
    
    func finishSession() {
        view.showFinishedState(title: "All done!", action: "Start over!")
        sessionStatus = .finished
    }
    
    func notEnoughWords() {
        view.showFinishedState(title: "You learned all of the existing words! Why not add some more?", action: nil)
        sessionStatus = .noWordsAvailable
    }
    
}
