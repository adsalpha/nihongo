//
//  QuizConfigurator.swift
//  nihongo
//
//  Created by Alex on 8/1/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol QuizConfiguratorProtocol: class {
    func configure(with viewController: QuizViewController)
}

class QuizConfigurator: QuizConfiguratorProtocol {
    
    func configure(with viewController: QuizViewController) {
        let presenter = QuizPresenter(view: viewController)
        let interactor = QuizInteractor(presenter: presenter)
        let router = QuizRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
    
}
