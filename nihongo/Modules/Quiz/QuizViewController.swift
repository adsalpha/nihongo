//
//  QuizViewController.swift
//  nihongo
//
//  Created by Alex on 8/1/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

protocol QuizViewProtocol: class {
    func deactivateOption(at optionIndex: Int)
    func showFinishedState(title: String, action: String?)
    func updateQuestionFields(primary: String, secondary: String?, options: [String], questionDescription: String)
    func showQuestion()
}

class QuizViewController: UIViewController, QuizViewProtocol {

    var presenter: QuizPresenterProtocol!
    let configurator: QuizConfiguratorProtocol = QuizConfigurator()
    
    var mainView: QuizView {
        return self.view as! QuizView
    }
    
    override func loadView() {
        self.view = QuizView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Quiz"
        configurator.configure(with: self)
    }
    
    private var shouldConfigure = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if shouldConfigure {
            let padding = self.mainView.safeAreaInsets.top + self.mainView.safeAreaInsets.bottom
            self.mainView.configureWithHeight(UIScreen.main.bounds.height - padding)
            self.mainView.questionView.buttonAction = buttonClicked
            shouldConfigure = false
        }
        
        presenter.configureView()
    }
    
    // MARK: - Quiz
    
    func buttonClicked(at optionIndex: Int) {
        presenter.optionClicked(withIndex: optionIndex)
    }
    
    func deactivateOption(at optionIndex: Int) {
        self.mainView.questionView.deactivateButton(at: optionIndex)
    }
    
    func updateQuestionFields(primary: String, secondary: String?, options: [String], questionDescription: String) {
        self.mainView.questionView.setWord(primary: primary, secondary: secondary)
        self.mainView.questionView.setButtonOptions(to: options)
        self.mainView.questionView.setDescription(question: questionDescription)
        self.mainView.questionView.resetAllButtons()
        self.mainView.reload()
    }
    
    // MARK: - Finished state
    
    func showFinishedState(title: String, action: String?) {
        self.mainView.showFinished(canStartOver: action != nil)
        if action != nil {
            self.mainView.finishedQuizView.setStartOverButtonAction(
                #selector(QuizViewController.startOverButtonClicked(_:)),
                forTarget: self
            )
        }
        self.mainView.finishedQuizView.setDescriptionText(title)
    }
    
    func showQuestion() {
        self.mainView.showQuestion()
    }

    @objc func startOverButtonClicked(_ sender: UIButton) {
        presenter.startOverButtonClicked()
    }
    
}
