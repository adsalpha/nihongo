//
//  SettingsInteractor.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol SettingsInteractorProtocol: class {
    var autoloadExamples: Bool { get set }
}

class SettingsInteractor: SettingsInteractorProtocol {
    
    weak var presenter: SettingsPresenterProtocol!
    
    required init(presenter: SettingsPresenterProtocol) {
        self.presenter = presenter
    }
    
    // MARK: - Settings
    
    let settingsService = SettingsService.shared
    
    var autoloadExamples: Bool {
        get {
            return settingsService.autoloadExamples
        }
        set {
            settingsService.autoloadExamples = newValue
        }
    }
    
}
