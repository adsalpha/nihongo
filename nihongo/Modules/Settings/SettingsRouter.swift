//
//  SettingsRouter.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol SettingsRouterProtocol: class {
    func showImport()
    func dismiss()
}

class SettingsRouter: SettingsRouterProtocol {
    
    weak var viewController: SettingsViewController!
    weak var outputPresenter: VocabularyPresenterProtocol!
    
    init(viewController: SettingsViewController) {
        self.viewController = viewController
    }
    
    func showImport() {
        viewController.navigationController?.pushViewController(ImportViewController(), animated: true)
    }
    
    func dismiss() {
        outputPresenter.dismissSettingsController()
    }
    
}
