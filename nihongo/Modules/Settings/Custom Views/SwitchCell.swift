//
//  SwitchCell.swift
//  nihongo
//
//  Created by Alex on 2018-09-02.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import FlexLayout

class SwitchCell: UITableViewCell {
    
    static let reuseIdentifier = "SwitchCell"
    
    // MARK: - Title
    
    var title: String? {
        didSet {
            textLabel?.text = title
        }
    }
    
    // MARK: - Description
    
    var detail: String? {
        didSet {
            detailTextLabel?.text = detail
        }
    }
    
    // MARK: - Switch
    
    private let switchControl = UISwitch()
    
    private func setupSwitchControl() {
        switchControl.addTarget(self, action: #selector(switchFlipped(_:)), for: .valueChanged)
    }
    
    var whenSwitchIsFlipped: (Bool) -> () = { _ in return }
    
    @objc private func switchFlipped(_ sender: UISwitch) {
        whenSwitchIsFlipped(sender.isOn)
    }
    
    func flipSwitch(to newValue: Bool) {
        switchControl.isOn = newValue
    }
    
    // MARK: - Init
    
    init() {
        super.init(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: SwitchCell.reuseIdentifier)
        
        self.selectionStyle = .none
        self.separatorInset = .zero
        
        self.detailTextLabel?.numberOfLines = 0
        
        self.accessoryView = switchControl
        setupSwitchControl()
    }
    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: reuseIdentifier)
//        self.backgroundColor = .white
//
//        self.selectionStyle = .none
//        self.separatorInset = .zero
//
//        setupDescriptionLabel()
//
//        self.textLabel?.text = "Hello"
//        self.detailTextLabel?.text = "Anime"
//        self.accessoryView = switchControl
//
////        contentView.flex.padding(8, 16, 8, 16).direction(.row).define {
////            flex in
////            flex.addItem().width(55%).define {
////                flex in
////                flex.addItem(titleLabel).marginBottom(8)
////                flex.addItem(descriptionLabel)
////            }
////            flex.addItem().direction(.column).justifyContent(.center).define {
////                flex in
////                flex.addItem(switchControl)
////            }
////        }
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Layout
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        layout()
//    }
//
//    private func layout() {
//        contentView.flex.layout(mode: .adjustHeight)
//    }
//
//    override func sizeThatFits(_ size: CGSize) -> CGSize {
//        contentView.pin.width(size.width)
//        layout()
//        return contentView.frame.size
//    }

}
