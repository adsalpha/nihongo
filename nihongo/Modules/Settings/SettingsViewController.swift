//
//  SettingsViewController.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

protocol SettingsViewProtocol: class {
    var autoloadExamples: Bool { get set }
}

class SettingsViewController: UITableViewController, SettingsViewProtocol {
    
    // Changing autoload examples is forbidden for now because the dataset is not ready
    
    var presenter: SettingsPresenterProtocol!
    let configurator: SettingsConfiguratorProtocol = SettingsConfigurator()
    
    // MARK: Table view cells
    
    let importCell = UITableViewCell()
//    let autoloadExamplesCell = SwitchCell()
    
    private func configureTableView() {
        importCell.textLabel?.text = "Import"
        importCell.accessoryType = .disclosureIndicator
//        autoloadExamplesCell.title = "Automatic examples"
//        autoloadExamplesCell.detail = "Load an example sentence when you \nadd a word, if available"
//        autoloadExamplesCell.whenSwitchIsFlipped = whenSwitchIsFlipped
    }
    
    func whenSwitchIsFlipped(to newValue: Bool) {
//        presenter.autoloadExamplesFlipped(to: newValue)
    }
    
    var autoloadExamples: Bool = false {
        didSet {
//            autoloadExamplesCell.flipSwitch(to: autoloadExamples)
        }
    }
    
    override func loadView() {
        super.loadView()
        self.tableView = UITableView(frame: self.tableView.frame, style: .grouped)
        configureTableView()
    }
    
    // MARK: Done button
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(with: self)
        self.title = "Settings"
        configureDoneButton()
        presenter.configureView()
    }
    
    private func configureDoneButton() {
        self.navigationController?.navigationBar.tintColor = Colors.crimsonRed
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(SettingsViewController.doneButtonClicked(_:)))
    }
    
    @objc func doneButtonClicked(_ sender: UIButton) {
        presenter.clickedDone()
    }
    
    // MARK: Table view delegate & data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Sync"
//        case 1:
//            return "Examples"
        default: fatalError("Unknown section")
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                return importCell
            default: fatalError("Unknown indexPath")
            }
//        case 1:
//            switch indexPath.row {
//            case 0:
//                return autoloadExamplesCell
//            default: fatalError("Unknown indexPath")
//            }
        default: fatalError("Unknown indexPath")
        }
    }
    
        
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                presenter.clickedImport()
            default: fatalError("Unknown indexPath")
            }
//        case 1:
//            switch indexPath.row {
//            case 0:
//                break
//            default: fatalError("Unknown indexPath")
//            }
        default: fatalError("Unknown indexPath")
        }
    }
    
}
