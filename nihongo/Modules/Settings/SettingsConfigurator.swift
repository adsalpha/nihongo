//
//  SettingsConfigurator.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol SettingsConfiguratorProtocol: class {
    func configure(with viewController: SettingsViewController)
}

class SettingsConfigurator: SettingsConfiguratorProtocol {
    
    func configure(with viewController: SettingsViewController) {
        let presenter = SettingsPresenter(view: viewController)
        let interactor = SettingsInteractor(presenter: presenter)
        let router = SettingsRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
    
}
