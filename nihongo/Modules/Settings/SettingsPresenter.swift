//
//  SettingsPresenter.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol SettingsPresenterProtocol: class {
    var router: SettingsRouterProtocol! { set get }
    func clickedImport()
    func clickedDone()
    func configureView()
    func autoloadExamplesFlipped(to newValue: Bool)
}

class SettingsPresenter: SettingsPresenterProtocol {
    
    weak var view: SettingsViewProtocol!
    var interactor: SettingsInteractorProtocol!
    var router: SettingsRouterProtocol!
    
    required init(view: SettingsViewProtocol) {
        self.view = view
    }
    
    func clickedImport() {
        router.showImport()
    }
    
    func clickedDone() {
        router.dismiss()
    }
    
    // MARK: - Examples autoloading
    
    func configureView() {
        view.autoloadExamples = interactor.autoloadExamples
    }
    
    func autoloadExamplesFlipped(to newValue: Bool) {
        interactor.autoloadExamples = newValue
    }
    
}
