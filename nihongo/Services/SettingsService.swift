//
//  SettingsService.swift
//  nihongo
//
//  Created by Alex on 2018-09-02.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol SettingsServiceProtocol: class {
    var autoloadExamples: Bool { get set }
    var isFirstLaunch: Bool { get }
}

enum Settings: String {
    case autoloadExamples = "autoloadExamples"
    case isFirstLaunch = "isFirstLaunch"
}

class SettingsService: SettingsServiceProtocol {
    
    static let shared: SettingsServiceProtocol = SettingsService()
    
    private let defaults = UserDefaults.standard
    
    var autoloadExamples: Bool {
        get {
            if defaults.value(forKey: Settings.autoloadExamples.rawValue) == nil {
                defaults.set(false, forKey: Settings.autoloadExamples.rawValue)
            }
            return defaults.value(forKey: Settings.autoloadExamples.rawValue) as! Bool
        }
        set {
            defaults.set(newValue, forKey: Settings.autoloadExamples.rawValue)
        }
    }
    
    var isFirstLaunch: Bool {
        get {
            if defaults.value(forKey: Settings.isFirstLaunch.rawValue) == nil {
                defaults.set(false, forKey: Settings.isFirstLaunch.rawValue)
                return true
            }
            return defaults.value(forKey: Settings.isFirstLaunch.rawValue) as! Bool
        }
    }
    
}
