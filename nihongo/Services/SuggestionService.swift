//
//  SuggestionService.swift
//  nihongo
//
//  Created by Alex on 8/24/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol SuggestionServiceProtocol {
    func getKanjiForKana(_ kana: String) -> Suggestion
    func getKanaForKanji(_ kanji: String) -> Suggestion
    func getTranslationsForWord(kana: String?, kanji: String?) -> Suggestion
    func getPartsOfSpeechForWord(kana: String, kanji: String?) -> Suggestion
    func getCategoriesForWord(kana: String?, kanji: String?) -> Suggestion
    func getExampleForWord(kana: String, kanji: String?) -> Suggestion
    func getJlptLevelForWord(kana: String, kanji: String?) -> Suggestion
}

class SuggestionService: SuggestionServiceProtocol {
    
    private let realmService = RealmService.shared
    
    func getKanjiForKana(_ kana: String) -> Suggestion {
        let words = realmService.getThreeSuggestionWords(kana: kana, kanji: nil)
        var results = [String]()
        for word in words {
            if let kanji = word.kanji {
                if !results.contains(kanji) {
                    results.append(kanji)
                }
            }
        }
        return Suggestion.kanjiForKana(results)
    }

    func getKanaForKanji(_ kanji: String) -> Suggestion {
        let words = realmService.getThreeSuggestionWords(kana: nil, kanji: kanji)
        var results = [String]()
        for word in words {
            if !results.contains(word.kana) {
                results.append(word.kana)
            }
        }
        return Suggestion.kanaForKanji(results)
    }

    func getTranslationsForWord(kana: String?, kanji: String?) -> Suggestion {
        let words = realmService.getThreeSuggestionWords(kana: kana, kanji: kanji)
        var results = [String]()
        for word in words {
            for translation in word.translations {
                if !results.contains(translation) {
                    results.append(translation)
                }
            }
        }
        return Suggestion.translation(results)
    }

    func getPartsOfSpeechForWord(kana: String, kanji: String?) -> Suggestion {
        let word = realmService.getExactSuggestionMatch(kana: kana, kanji: kanji)
        return Suggestion.partsOfSpeech(word?.partsOfSpeech.shuffled() ?? [])
    }
    
    func getJlptLevelForWord(kana: String, kanji: String?) -> Suggestion {
        let word = realmService.getExactSuggestionMatch(kana: kana, kanji: kanji)
        return Suggestion.jlptLevel(word?.jlptLevel)
    }

    func getCategoriesForWord(kana: String?, kanji: String?) -> Suggestion {
        let words = realmService.getThreeSuggestionWords(kana: kana, kanji: kanji)
        var results = [String]()
        for word in words {
            for category in word.categories {
                if !results.contains(category) {
                    results.append(category)
                }
            }
        }
        return Suggestion.category(results)
    }
    
    func getExampleForWord(kana: String, kanji: String?) -> Suggestion {
        return Suggestion.examples([("", "")])
    }
    
}

