//
//  ProcessImportService.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import Alamofire

protocol ProcessImportServiceProtocol: class {
    func importWordsFromUrl(_ url: URL, duplicateErrorHandler: @escaping ([Word]) -> (), internalErrorHandler: @escaping (String) -> (), successHandler: @escaping () -> ())
}

class ProcessImportService: ProcessImportServiceProtocol {
    
    let realmService = RealmService.shared
    let jsonParsingService: JsonParsingServiceProtocol = JsonParsingService()
    
    func importWordsFromUrl(_ url: URL, duplicateErrorHandler: @escaping ([Word]) -> (), internalErrorHandler: @escaping (String) -> (), successHandler: @escaping () -> ()) {
        Alamofire.request(url).responseData {
            [weak self] response in
            switch response.result {
            case .success(let data):
                if let self = self {
                    if let words = self.jsonParsingService.parseImportedJson(from: data) {
                        var duplicates = [Word]()
                        for word in words {
                            // If there is a kanji + kana match in the DB most likely the word is a duplicate
                            // Can't use only one of them as there are many words w/o kanji and words with repeating kana
                            // These conditions are somewhat overlapping and probably can be improved by making isKanjiDuplicate lazy-ish
                            let wordHasKanji = word.kanji != nil
                            let isKanjiDuplicate = word.kanji != nil && self.realmService.kanjiExists(word.kanji!)
                            let isKanaDuplicate = self.realmService.kanaExists(word.kana)

                            if (wordHasKanji && !isKanjiDuplicate && !isKanaDuplicate) || (!wordHasKanji && !isKanaDuplicate) {
                                self.realmService.addWord(word)
                            } else {
                                duplicates.append(word)
                            }
                        }
                        if duplicates.count != 0 {
                            duplicateErrorHandler(duplicates)
                        }
                        successHandler()
                    } else { internalErrorHandler("JSON cannot be parsed, check the URL") }
                }
            case .failure(let error):
                internalErrorHandler(error.localizedDescription)
            }
           
        }
    }
    
}
