//
//  RealmService.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmServiceProtocol: class {    
    func addWord(_ word: Word)
    func getAllWords() -> [Word]
    func getWord(identifiedBy id: Int) -> Word?
    func getUpTo20RandomWordsWithNonzeroWeights() -> [Word]
    func kanjiExists(_ kanji: String) -> Bool
    func kanaExists(_ kana: String) -> Bool
    func decreaseWordWeight(_ word: Word)
    func increaseWordWeight(_ word: Word)
    func updateWord(_ word: Word, withContentsOf contents: Word)
    var deletionLock: Bool { get set }
    func deleteWord(withId id: Int)
    
    func getThreeSuggestionWords(kana: String?, kanji: String?) -> [Word]
    func getExactSuggestionMatch(kana: String, kanji: String?) -> Word?
}

class RealmService: RealmServiceProtocol {
    
    static let shared: RealmServiceProtocol = RealmService()
    
    static let schemaVersion: UInt64 = 7
    
    // DEFAULT
    
    static let defaultConfig = Realm.Configuration(
        schemaVersion: RealmService.schemaVersion
    )
    
    // Create
    
    func addWord(_ word: Word) {
        let realm = try! Realm()
        let lastId = realm.objects(Word.self).sorted(byKeyPath: "id").last?.id ?? 0
        word.id = lastId + 1
        try? realm.write {
            realm.add(word)
        }
    }
    
    // Read
    
    func resultsToArray(_ results: Results<Word>) -> [Word] {
        var words = [Word]()
        for word in results {
            words.append(word)
        }
        return words
    }
    
    func getAllWords() -> [Word] {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "visible = YES")
        let words = realm.objects(Word.self).filter(predicate)
        return resultsToArray(words)
    }
    
    func getFirstWord(withPredicate predicate: NSPredicate) -> Word? {
        let realm = try! Realm()
        return realm.objects(Word.self).filter(predicate).first
    }
    
    func getWord(identifiedBy id: Int) -> Word? {
        let predicate = NSPredicate(format: "id = %d", id)
        return getFirstWord(withPredicate: predicate)
    }
    
    func getUpTo20RandomWordsWithNonzeroWeights() -> [Word] {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "weight != 0")
        var words = realm.objects(Word.self).filter(predicate).shuffled()
        if words.count > 20 {
            return Array(words[0..<20])
        }
        return words
    }
    
    func kanjiExists(_ kanji: String) -> Bool {
        let predicate = NSPredicate(format: "kanji = %@", kanji)
        return getFirstWord(withPredicate: predicate) != nil
    }
    
    func kanaExists(_ kana: String) -> Bool {
        let predicate = NSPredicate(format: "kana = %@", kana)
        return getFirstWord(withPredicate: predicate) != nil
    }
    
    // Update
    
    func decreaseWordWeight(_ word: Word) {
        let realm = try! Realm()
        try! realm.write {
            word.weight -= 1
        }
    }
    
    func increaseWordWeight(_ word: Word) {
        let realm = try! Realm()
        try! realm.write {
            word.weight += 1
        }
    }
    
    func updateWord(_ word: Word, withContentsOf contents: Word) {
        let realm = try! Realm()
        try! realm.write {
            word.kana = contents.kana
            word.kanji = contents.kanji
            word.translations.removeAll()
            word.translations.append(objectsIn: contents.translations)
            word.partsOfSpeech.removeAll()
            word.partsOfSpeech.append(objectsIn: contents.partsOfSpeech)
            word.categories.removeAll()
            word.categories.append(objectsIn: contents.categories)
            word.examples.removeAll()
            word.examples.append(objectsIn: contents.examples)
        }
    }
    
    // Delete
    
    // Necessary in case user tries to delete a word while a quiz session is in progress
    var deletionLock: Bool = false
    
    func deleteWord(withId id: Int) {
        if !deletionLock {
            if let word = getWord(identifiedBy: id) {
                let realm = try! Realm()
                try! realm.write {
                    realm.delete(word)
                }
            }
        }
    }
    
    // SUGGESTIONS
    
    static let suggestionsConfig = Realm.Configuration(
        fileURL: FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("words.realm"),
        schemaVersion: schemaVersion
    )
    
    func getThreeSuggestionWords(kana: String?, kanji: String?) -> [Word] {
        let realm = try! Realm(configuration: RealmService.suggestionsConfig)
        let predicate = NSPredicate(format: "kana BEGINSWITH %@ OR kanji BEGINSWITH %@", kana ?? "", kanji ?? "")
        let words = realm.objects(Word.self).filter(predicate)
        if words.count > 3 {
            return Array(words[0..<3])
        }
        return self.resultsToArray(words)
    }
    
    func getExactSuggestionMatch(kana: String, kanji: String?) -> Word? {
        let realm = try! Realm(configuration: RealmService.suggestionsConfig)
        let predicate = NSPredicate(format: "kana = %@ AND kanji = %@", kana, kanji ?? "")
        return realm.objects(Word.self).filter(predicate).first
    }

}
