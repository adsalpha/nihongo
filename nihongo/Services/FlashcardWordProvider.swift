//
//  FlashcardWordProvider.swift
//  nihongo
//
//  Created by Alex on 8/2/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol FlashcardWordProviderProtocol: class {
    func updateWords()
    func getNewWord() -> (Word?, FlashcardWordProviderError?)
    func get3RandomWords() -> ([Word]?, FlashcardWordProviderError?)
    func currentWordLearned()
    func currentWordNotLearned()
}

enum FlashcardWordProviderError {
    case notEnoughWords
    case noMoreWords
}

class FlashcardWordProvider: FlashcardWordProviderProtocol {
    
    let realmService = RealmService.shared
    
    var wordQueue: [Word]!
    var currentWord: Word!
    var currentIndex = 0
    
    func updateWords() {
        wordQueue = realmService.getUpTo20RandomWordsWithNonzeroWeights()
        currentIndex = 0
    }
    
    func getNewWord() -> (Word?, FlashcardWordProviderError?) {
        guard wordQueue.count >= 4 else {
            return (nil, .notEnoughWords)
        }
        
        if wordQueue.count >= currentIndex + 1 {
            currentWord = wordQueue[currentIndex]
            currentIndex += 1
            return (currentWord, nil)
        } else {
            return (nil, .noMoreWords)
        }
    }
    
    func wordIsInArray(word: Word, array: [Word]) -> Bool {
        for entry in array {
            if entry.id == word.id {
                return true
            }
        }
        return false
    }
    
    func get3RandomWords() -> ([Word]?, FlashcardWordProviderError?) {
        guard wordQueue.count >= 3 && currentWord != nil else {
            return (nil, .notEnoughWords)
        }
        
        var randomWords = [Word]()
        var randomWord: Word!
        for _ in 0...2 {
            repeat {
                randomWord = wordQueue.randomElement()!
            } while randomWords.contains(randomWord) || randomWord.id == currentWord.id
            randomWords.append(randomWord)
        }
        return (randomWords, nil)
    }
    
    func currentWordLearned() {
        realmService.decreaseWordWeight(currentWord)
    }
    
    func currentWordNotLearned() {
        realmService.increaseWordWeight(currentWord)
    }
    
}
