//
//  JsonParsingService.swift
//  nihongo
//
//  Created by Alex on 7/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

protocol JsonParsingServiceProtocol: class {
    func parseImportedJson(from data: Data) -> [Word]?
}

class JsonParsingService: JsonParsingServiceProtocol {
    
    func parseImportedJson(from data: Data) -> [Word]? {
        let jsonDecoder = JSONDecoder()
        do {
            let words = try jsonDecoder.decode([Word].self, from: data)
            return words
        } catch {
            return nil
        }
    }

}
