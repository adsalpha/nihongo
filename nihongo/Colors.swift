//
//  Colors.swift
//  nihongo
//
//  Created by Alex on 8/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

struct Colors {
    static let tableViewBackgroundGray = UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1.0)
    static let tableViewHeaderTextGray = UIColor(red: 0.43, green: 0.43, blue: 0.45, alpha: 1.0)
    static let secondaryTextGray = UIColor(red: 0.4, green: 0.4, blue: 0.3, alpha: 1.0)
    static let inactiveInputGray = UIColor(red: 0.61, green: 0.61, blue: 0.61, alpha: 1.0)
    static let crimsonRed = UIColor(red: 0.60, green: 0.00, blue: 0.00, alpha: 1.0)
    static let optionButtonGray = UIColor(red: 0.67, green: 0.66, blue: 0.66, alpha: 1.0)
    static let suggestionViewGray = UIColor(red: 0.82, green: 0.83, blue: 0.85, alpha: 0.9)
}
