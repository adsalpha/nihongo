//
//  UIViewController+showSimpleAlert.swift
//  nihongo
//
//  Created by Alex on 8/2/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showSimpleAlert(title: String, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}
