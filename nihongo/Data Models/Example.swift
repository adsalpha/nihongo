//
//  Example.swift
//  nihongo
//
//  Created by Alex on 2018-09-09.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

struct ExampleVM {
    
    var jp: String
    var en: String
    
}

class Example: Object, Decodable {
    
    @objc dynamic var jp: String = ""
    @objc dynamic var en: String = ""
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    // MARK: - Codable
    
    private enum CodingKeys: String, CodingKey {
        case jp
        case en
    }
    
    convenience init(jp: String, en: String) {
        self.init()
        self.jp = jp
        self.en = en
    }
    
    convenience required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let jp = try container.decode(String.self, forKey: .jp)
        let en = try container.decode(String.self, forKey: .en)
        self.init(jp: jp, en: en)
    }
    
    // MARK: - View Model
    
    func toVM() -> ExampleVM {
        return ExampleVM(jp: jp, en: en)
    }
    
}
