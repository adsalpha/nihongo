//
//  Word.swift
//  nihongo
//
//  Created by Alex on 7/30/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

struct WordVM {
    
    var id: Int
    var kana: String
    var kanji: String?
    var translations: [String]
    var partsOfSpeech: [String]
    var categories: [String]
    var weight: Int
    var examples: [ExampleVM]
    var jlptLevel: String?
    
}

class Word: Object, Decodable {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var kana: String = ""
    @objc dynamic var kanji: String? = nil
    let translations = List<String>()
    let partsOfSpeech = List<String>()
    let categories = List<String>()
    @objc dynamic var weight: Int = 3
    let examples = List<Example>()
    @objc dynamic var jlptLevel: String? = ""
    @objc dynamic var visible: Bool = false
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    // MARK: - Codable
    
    private enum CodingKeys: String, CodingKey {
        case kana
        case kanji
        case translations
        case partsOfSpeech = "pos"
        case categories
        case examples
        case jlptLevel = "jlpt"
    }
    
    convenience init(kana: String, kanji: String?, translations: [String], partsOfSpeech: [String], categories: [String], examples: [Example], jlptLevel: String?, visible: Bool) {
        self.init()
        self.kana = kana
        self.kanji = kanji
        self.translations.append(objectsIn: translations)
        self.partsOfSpeech.append(objectsIn: partsOfSpeech)
        self.categories.append(objectsIn: categories)
        self.examples.append(objectsIn: examples)
        self.jlptLevel = jlptLevel
        self.visible = visible
    }
    
    convenience required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let kana = try container.decode(String.self, forKey: .kana)
        let kanji = try? container.decode(String.self, forKey: .kanji)
        let translations = try container.decode(Array<String>.self, forKey: .translations)
        let partsOfSpeech = try container.decode(Array<String>.self, forKey: .partsOfSpeech)
        let categories = try container.decode(Array<String>.self, forKey: .categories)
        let examples = try container.decode(Array<Example>.self, forKey: .examples)
        let jlptLevel = try? container.decode(String.self, forKey: .jlptLevel)
        let visible = false
        self.init(kana: kana, kanji: kanji, translations: translations, partsOfSpeech: partsOfSpeech, categories: categories, examples: examples, jlptLevel: jlptLevel, visible: visible)
    }
    
    // MARK: - View Model
    
    func toVM() -> WordVM {
        var translations = [String](), partsOfSpeech = [String](), categories = [String]()
        translations.append(contentsOf: self.translations)
        
        for pos in self.partsOfSpeech {
            partsOfSpeech.append(WordConstants.categoriesAndPosMap[pos]!)
        }

        categories.append(contentsOf: self.categories)
        
        var examples = [ExampleVM]()
        for example in self.examples {
            examples.append(example.toVM())
        }
        
        return WordVM(id: id, kana: kana, kanji: kanji, translations: translations, partsOfSpeech: partsOfSpeech, categories: categories, weight: weight, examples: examples, jlptLevel: jlptLevel)
    }

}
