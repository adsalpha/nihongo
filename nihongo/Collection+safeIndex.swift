//
//  Collection+safeIndex.swift
//  nihongo
//
//  Created by Alex on 2018-09-02.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
