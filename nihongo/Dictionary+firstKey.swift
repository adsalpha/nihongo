//
//  Dictionary+firstKey.swift
//  nihongo
//
//  Created by Alex on 8/12/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

// Borrowed from https://stackoverflow.com/questions/27218669/swift-dictionary-get-key-for-value, dfri's answer

extension Dictionary where Value: Equatable {
    func firstKey(forValue val: Value) -> Key? {
        return first(where: { $1 == val })?.key
    }
}
